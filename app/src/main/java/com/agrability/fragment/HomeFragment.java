package com.agrability.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.ArraySet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.agrability.R;
import com.agrability.activity.HomeSubActivity;
import com.agrability.activity.MyFavoritesActivity;
import com.agrability.activity.SubSubCategoriesActivity;
import com.agrability.adapter.FavoritesAdapter;
import com.agrability.adapter.HomeFragmentListBaseAdapter;
import com.agrability.databinding.FragmentHomeBinding;
import com.agrability.model.GetCategories.CategoriesItem;
import com.agrability.model.GetCategories.GetCategoriesResponse;
import com.agrability.model.GetFavoritesError.GetFavoritesErrorResponse;
import com.agrability.model.GetFavoritesResponse.GetFavoritesResponse;
import com.agrability.model.nonce.GenerateNonceFavorites;
import com.agrability.retrofit.APIClient;
import com.agrability.retrofit.APIInterface;
import com.agrability.utils.AndyConstants;
import com.agrability.utils.AndyUtils;
import com.agrability.utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "HomeFragment";
    private FragmentHomeBinding fragmentHomeBinding;
    private View view;
    private ArrayList<CategoriesItem> categoriesItems = new ArrayList<>();
    private APIInterface apiInterface;
    private PreferenceHelper preferenceHelper;
    private ArrayList<GetFavoritesResponse> getFavoritesResponseArrayList = new ArrayList<>();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        preferenceHelper = new PreferenceHelper(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentHomeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        view = fragmentHomeBinding.getRoot();
        toolBar();
        init();
//        generateNonce();
        getAllCategories();
        return view;
    }

    private void getAllCategories() {
        if (AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showSimpleProgressDialog(getActivity());
            Call<GetCategoriesResponse> loginCall = apiInterface.getAllCategories();
            loginCall.enqueue(new Callback<GetCategoriesResponse>() {
                @Override
                public void onResponse(Call<GetCategoriesResponse> call, Response<GetCategoriesResponse> response) {
                    AndyUtils.removeSimpleProgressDialog();
                    if (response.body() == null) {
                        AndyUtils.showToast("Empty Response", getActivity());
                        return;
                    } else if (response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {

                        preferenceHelper.putAllCategories(response.body());
                        categoriesItems.clear();
                        for (CategoriesItem categoriesItem : response.body().getCategories()){
                            if(categoriesItem.getParent() == 0) {
                                categoriesItems.add(categoriesItem);
                            }
                        }
                        CategoriesItem categoriesItem = new CategoriesItem();
                        categoriesItem.setDescription("");
                        categoriesItem.setTitle("View All");
                        categoriesItem.setSlug("activity");
                        categoriesItem.setParent(0);
                        categoriesItem.setId(-1);
                        categoriesItems.add(categoriesItem);
//                        Log.d(TAG, "onResponse: "+categoriesItems.size());
                        fragmentHomeBinding.mGridView.setAdapter(new HomeFragmentListBaseAdapter(getActivity(), categoriesItems));

                    } else if (!response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        AndyUtils.showToast("response not ok", getActivity());
                    }
                }

                @Override
                public void onFailure(Call<GetCategoriesResponse> call, Throwable t) {
                    AndyUtils.removeSimpleProgressDialog();
                }
            });
        } else {
            AndyUtils.DialogForNoInternetConnection(getActivity());
        }
    }

    private void toolBar() {

    }

    private void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View headerView = layoutInflater.inflate(R.layout.home_grid_header, null);
        fragmentHomeBinding.mGridView.addHeaderView(headerView);


        fragmentHomeBinding.mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AndyConstants.setIdOfMainCategory(categoriesItems.get(position).getId());
                AndyConstants.setCategoryTitle(categoriesItems.get(position).getTitle());
                if(AndyConstants.getIdOfMainCategory() == -1){
                    AndyConstants.setCategoryIDForSolution(categoriesItems.get(position).getId());
                    AndyUtils.openActivity(getActivity(), SubSubCategoriesActivity.class);
                }else {
                    AndyUtils.openActivity(getActivity(), HomeSubActivity.class);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            default:
                break;

        }
    }

    /*//region API CALL
    private void generateNonce() {
        if (AndyUtils.isNetworkAvailable(getActivity())) {
//            AndyUtils.showSimpleProgressDialog(getActivity());
            Call<GenerateNonceFavorites> loginCall = apiInterface.generateNonceFavorite();
            loginCall.enqueue(new Callback<GenerateNonceFavorites>() {
                @Override
                public void onResponse(Call<GenerateNonceFavorites> call, Response<GenerateNonceFavorites> response) {
//                    AndyUtils.removeSimpleProgressDialog();
                    if (response.body() == null) {
                        AndyUtils.showToast("Empty Response", getActivity());
                        return;
                    } else if (response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        preferenceHelper.putFavNonce(response.body().getNonce());
                        getFavoriteList(response.body().getNonce(), preferenceHelper.getUserProfile().getCookie());
                    } else if (!response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        AndyUtils.showToast("response not ok", getActivity());
                    }
                }

                @Override
                public void onFailure(Call<GenerateNonceFavorites> call, Throwable t) {

                }
            });
        } else {
            AndyUtils.DialogForNoInternetConnection(getActivity());
        }
    }

    private void getFavoriteList(String nonce, String cookie) {
        if (AndyUtils.isNetworkAvailable(getActivity())) {
//            AndyUtils.showSimpleProgressDialog(getActivity());
            Call<List<GetFavoritesResponse>> loginCall = apiInterface.getFavorites(nonce, cookie);
            loginCall.enqueue(new Callback<List<GetFavoritesResponse>>() {
                @Override
                public void onResponse(Call<List<GetFavoritesResponse>> call, Response<List<GetFavoritesResponse>> response) {
//                    AndyUtils.removeSimpleProgressDialog();
                    if (response.body() == null) {
                        AndyUtils.showToast("Empty Response", getActivity());
                        return;
                    } else if (response.body().size() != 0 && response.body().get(0) != null) {
                        List<GetFavoritesResponse> getFavoritesResponse = (List<GetFavoritesResponse>) response.body();
                        Log.d(TAG, "onResponse:FAV1 "+getFavoritesResponse);
                        getFavoritesResponseArrayList.clear();
                        getFavoritesResponseArrayList.addAll(getFavoritesResponse);

//                        Log.d(TAG, "onResponse:FAV2 "+getFavoritesResponseArrayList.size());
//                        myFavoritesBinding.mGridViewFavorites.setAdapter(new FavoritesAdapter(getActivity(),
//                                getFavoritesResponseArrayList));
                    } else if (response.body() instanceof GetFavoritesErrorResponse) {
                        GetFavoritesErrorResponse getFavoritesErrorResponse = (GetFavoritesErrorResponse) response.body();
                        AndyUtils.showToast(getFavoritesErrorResponse.getMessage(), getActivity());
                    } else {
                        Log.d(TAG, "onResponse:else ");
                    }
                }

                @Override
                public void onFailure(Call<List<GetFavoritesResponse>> call, Throwable t) {

                    Log.d(TAG, "onFailure: "+t.getMessage());
                }
            });
        } else {
            AndyUtils.DialogForNoInternetConnection(getActivity());
        }
    }
    //endregion*/
}
