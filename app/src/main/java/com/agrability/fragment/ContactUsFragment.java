package com.agrability.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agrability.R;
import com.agrability.databinding.FragmentContactUsBinding;
import com.agrability.utils.AndyUtils;

public class ContactUsFragment extends Fragment {

    private static final String TAG = "ContactUsFragment";
    private View view;
    private FragmentContactUsBinding fragmentContactUsBinding;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentContactUsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact_us, container, false);
        view = fragmentContactUsBinding.getRoot();

        fragmentContactUsBinding.mBtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(fragmentContactUsBinding.mEdtSubject.getText().toString().trim())) {
                    AndyUtils.showToast("please enter Subject", getActivity());
                } else if (TextUtils.isEmpty(fragmentContactUsBinding.mEdtMessage.getText().toString().trim())) {
                    AndyUtils.showToast("Please enter message", getActivity());
                } else {
                    Log.d(TAG, "shareinmail: ");
                    AndyUtils.shareWithEmail(getContext(),
                            fragmentContactUsBinding.mEdtSubject.getText().toString(),
                            fragmentContactUsBinding.mEdtMessage.getText().toString());
                }
            }
        });
        return view;
    }

}
