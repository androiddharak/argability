package com.agrability.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agrability.R;
import com.agrability.databinding.FragmentLinksBinding;
import com.agrability.utils.AndyConstants;

public class LinksFragment extends Fragment{

    private View view;
    private FragmentLinksBinding fragmentLinksBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentLinksBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_links, container, false);
        view = fragmentLinksBinding.getRoot();

        fragmentLinksBinding.mButtonOne.setText("http://www.growingwarriors.org/");
        fragmentLinksBinding.mButtonTwo.setText("https://www.pliablemind.com");

        fragmentLinksBinding.mIvBack.setVisibility(View.GONE);

        fragmentLinksBinding.mButtonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentLinksBinding.mWebView.setVisibility(View.VISIBLE);
                fragmentLinksBinding.mIvBack.setVisibility(View.VISIBLE);
                fragmentLinksBinding.mButtonOne.setVisibility(View.GONE);
                fragmentLinksBinding.mButtonTwo.setVisibility(View.GONE);
                fragmentLinksBinding.mWebView.loadUrl("http://www.growingwarriors.org/");
            }
        });

        fragmentLinksBinding.mButtonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentLinksBinding.mWebView.setVisibility(View.VISIBLE);
                fragmentLinksBinding.mIvBack.setVisibility(View.VISIBLE);
                fragmentLinksBinding.mButtonOne.setVisibility(View.GONE);
                fragmentLinksBinding.mButtonTwo.setVisibility(View.GONE);
                fragmentLinksBinding.mWebView.loadUrl("https://www.pliablemind.com");
            }
        });

        fragmentLinksBinding.mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                fragmentLinksBinding.mWebView.clearHistory();
                fragmentLinksBinding.mWebView.setVisibility(View.GONE);
                fragmentLinksBinding.mIvBack.setVisibility(View.GONE);
                fragmentLinksBinding.mButtonOne.setVisibility(View.VISIBLE);
                fragmentLinksBinding.mButtonTwo.setVisibility(View.VISIBLE);
            }
        });

        return view;
    }

}
