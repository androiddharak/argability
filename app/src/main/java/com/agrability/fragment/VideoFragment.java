package com.agrability.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.agrability.R;
import com.agrability.activity.SubSubCategoriesActivity;
import com.agrability.adapter.ActivityAdapter;
import com.agrability.adapter.DisabilityAdapter;
import com.agrability.adapter.EnterpriseAdapter;
import com.agrability.adapter.VideoAdapter;
import com.agrability.databinding.FragmentVideoBinding;
import com.agrability.model.GetAllVideoResponse.Meta;
import com.agrability.model.GetCategories.CategoriesItem;
import com.agrability.parse.AsyncTaskCompleteListener;
import com.agrability.parse.HttpRequester;
import com.agrability.utils.AndyConstants;
import com.agrability.utils.AndyUtils;
import com.agrability.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class VideoFragment extends Fragment implements AsyncTaskCompleteListener, View.OnClickListener, AdapterView.OnItemSelectedListener {

    private static final String TAG = "VideoFragment";
    private FragmentVideoBinding fragmentVideoBinding;
    private ArrayList<Meta> metaArrayList = new ArrayList<>();
    private PreferenceHelper preferenceHelper;
    private ArrayList<CategoriesItem> activityList = new ArrayList<>();
    private ArrayList<CategoriesItem> enterpriseList = new ArrayList<>();
    private ArrayList<CategoriesItem> disabilityList = new ArrayList<>();
    private EnterpriseAdapter enterpriseAdapter;
    private ActivityAdapter activityAdapter;
    private DisabilityAdapter disabilityAdapter;
    private int categoryActivity = -2, categoryEnterprice = -2, categoryDisablity = -2;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferenceHelper = new PreferenceHelper(getActivity());
        getAllVideo();
    }

    private void getAllVideo() {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.removeSimpleProgressDialog();
            AndyUtils.DialogForNoInternetConnection(getActivity());

            return;
        } else {
            AndyUtils.showSimpleProgressDialog(getActivity());
            HashMap<String, String> map = new HashMap<>();
            map.put(AndyConstants.URL, AndyConstants.Action.GET_ALL_VIDEOS);
            new HttpRequester(getActivity(), map, AndyConstants.ServiceCode.SOLUTION_BY_CATEGORY, true, this);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentVideoBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_video, container, false);
        return fragmentVideoBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        setAllSpinnerList();
    }

    private void setAllSpinnerList() {
        CategoriesItem categoriesItemNone = new CategoriesItem();
        categoriesItemNone.setTitle("none");
        categoriesItemNone.setId(-2);

        activityList.add(categoriesItemNone);
        enterpriseList.add(categoriesItemNone);
        disabilityList.add(categoriesItemNone);
        for (CategoriesItem categoriesItem : preferenceHelper.getAllCategories().getCategories()) {
            if (30 == categoriesItem.getParent()) {
                activityList.add(categoriesItem);
            } else if (categoriesItem.getParent() == 32) {
                enterpriseList.add(categoriesItem);
            } else if (categoriesItem.getParent() == 29) {
                disabilityList.add(categoriesItem);
            }
        }
        activityAdapter = new ActivityAdapter(getActivity(), activityList);
        fragmentVideoBinding.mSpinnerActivity.setAdapter(activityAdapter);

        enterpriseAdapter = new EnterpriseAdapter(getActivity(), enterpriseList);
        fragmentVideoBinding.mSpinnerEnterprise.setAdapter(enterpriseAdapter);

        disabilityAdapter = new DisabilityAdapter(getActivity(), disabilityList);
        fragmentVideoBinding.mSpinnerDisability.setAdapter(disabilityAdapter);
    }

    private void init() {
        fragmentVideoBinding.mIvFilter.setOnClickListener(this);
        fragmentVideoBinding.mIvDelete.setOnClickListener(this);
        fragmentVideoBinding.mBtnApply.setOnClickListener(this);

        fragmentVideoBinding.mSpinnerActivity.setOnItemSelectedListener(this);
        fragmentVideoBinding.mSpinnerEnterprise.setOnItemSelectedListener(this);
        fragmentVideoBinding.mSpinnerDisability.setOnItemSelectedListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mIvFilter:
                if (fragmentVideoBinding.mLinearLayoutSpinner.getVisibility() == View.VISIBLE) {
                    fragmentVideoBinding.mLinearLayoutSpinner.setVisibility(View.GONE);
                } else {
                    fragmentVideoBinding.mLinearLayoutSpinner.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.mIvDelete:
                setAllSpinnerList();
                getAllVideo();
                fragmentVideoBinding.mLinearLayoutSpinner.setVisibility(View.GONE);
                break;
            case R.id.mBtnApply:
                if (categoryActivity != -2 || categoryEnterprice != -2 || categoryDisablity != -2) {
                    getFilter(categoryActivity != -2 ? String.valueOf(categoryActivity) : "",
                            categoryEnterprice != -2 ? String.valueOf(categoryEnterprice) : "",
                            categoryDisablity != -2 ? String.valueOf(categoryDisablity) : "");
                } else {
                    AndyUtils.showToast("Please select one of filter options", getActivity());
                }
                break;
            default:
                break;
        }
    }

    private void getFilter(String categoryActivity, String categoryEnterprice, String categoryDisablity) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.removeSimpleProgressDialog();
            AndyUtils.DialogForNoInternetConnection(getActivity());

            return;
        } else {
            AndyUtils.showSimpleProgressDialog(getActivity());
            HashMap<String, String> map = new HashMap<>();
            map.put(AndyConstants.URL, AndyConstants.Action.FILTER_VIDEO_BY_CATEGORY
                    + AndyConstants.Params.CATEGORY_AND + "=" + categoryActivity
                    + "&" + AndyConstants.Params.CATEGORY_AND + "=" + categoryEnterprice
                    + "&" + AndyConstants.Params.CATEGORY_AND + "=" + categoryDisablity);


            new HttpRequester(getActivity(), map, AndyConstants.ServiceCode.SOLUTION_BY_CATEGORY, true, this);
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        AndyUtils.removeSimpleProgressDialog();
        Log.d(TAG, "onTaskCompleted: " + response);
        switch (serviceCode) {
            case AndyConstants.ServiceCode.SOLUTION_BY_CATEGORY:
                if (response != null) {
                    metaArrayList = getVideoParse(response, metaArrayList);
                    Log.d(TAG, "onTaskCompleted: " + metaArrayList.size());
//                    fragmentVideoBinding.mRecyclerViewVideo.setLayoutManager(new LinearLayoutManager(getActivity()));
//                    ThumbnailAdapter thumbnailAdapter = new ThumbnailAdapter(getActivity(), metaArrayList);
//                    fragmentVideoBinding.mRecyclerViewVideo.setAdapter(thumbnailAdapter);

                    fragmentVideoBinding.mRecyclerViewVideo.setHasFixedSize(true);
                    //to use RecycleView, you need a layout manager. default is LinearLayoutManager
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    fragmentVideoBinding.mRecyclerViewVideo.setLayoutManager(linearLayoutManager);
                    VideoAdapter adapter = new VideoAdapter(getActivity(), metaArrayList);
                    fragmentVideoBinding.mRecyclerViewVideo.setAdapter(adapter);
                    fragmentVideoBinding.mLinearLayoutSpinner.setVisibility(View.GONE);
                } else {
                    AndyUtils.showToast(getResources().getString(R.string.strServerNot), getActivity());
                }
                break;
            default:
                break;
        }
    }

    private ArrayList<Meta> getVideoParse(String response, ArrayList<Meta> metaArrayList) {
        metaArrayList.clear();

        try {
            JSONArray jsonArrayMain = new JSONArray(response);
            for (int i = 0; i < jsonArrayMain.length(); i++) {
                JSONObject jsonObjectChild = jsonArrayMain.optJSONObject(i);
                Meta meta = new Meta();
                meta.setVideoUrl(jsonObjectChild.getJSONObject("meta").getJSONArray("video_url").getString(0));
                meta.setVideoTitle(jsonObjectChild.getJSONObject("meta").getJSONArray("video_title").getString(0));
                meta.setYoutubeVideoId(jsonObjectChild.getJSONObject("meta").getJSONArray("youtube_video_id").getString(0));
                meta.setYoutubeVideoEmbedCode(jsonObjectChild.getJSONObject("meta").getJSONArray("youtube_video_embed_code").getString(0));
                metaArrayList.add(meta);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return metaArrayList;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.mSpinnerActivity:
                categoryActivity = activityList.get(position).getId();
                break;
            case R.id.mSpinnerEnterprise:
                categoryEnterprice = enterpriseList.get(position).getId();
                break;
            case R.id.mSpinnerDisability:
                categoryDisablity = disabilityList.get(position).getId();
                break;
            default:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
