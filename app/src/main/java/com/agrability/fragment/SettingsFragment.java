package com.agrability.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agrability.R;
import com.agrability.activity.ChangePasswordActivity;
import com.agrability.activity.MyFavoritesActivity;
import com.agrability.activity.MyProfileActivity;
import com.agrability.databinding.FragmentSettingsBinding;
import com.agrability.utils.AndyConstants;
import com.agrability.utils.AndyUtils;
import com.agrability.utils.PreferenceHelper;

public class SettingsFragment extends Fragment implements View.OnClickListener {


    private FragmentSettingsBinding fragmentSettingsBinding;
    private View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSettingsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);
        view = fragmentSettingsBinding.getRoot();
        clickEvent();
        return view;
    }

    private void clickEvent() {
        fragmentSettingsBinding.mButtonMyProfile.setOnClickListener(this);
        fragmentSettingsBinding.mButtonChangePassword.setOnClickListener(this);
        fragmentSettingsBinding.mButtonLogout.setOnClickListener(this);
        fragmentSettingsBinding.mButtonAboutUs.setOnClickListener(this);
        fragmentSettingsBinding.mButtonMyFavorite.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mButtonLogout:
                AndyUtils.LogoutDialog(getActivity(), new PreferenceHelper(getActivity()));
                break;
            case R.id.mButtonMyProfile:
                AndyUtils.openActivity(getActivity(), MyProfileActivity.class);
                break;
            case R.id.mButtonChangePassword:
                AndyUtils.openActivity(getActivity(), ChangePasswordActivity.class);
                break;
            case R.id.mButtonAboutUs:
                getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AndyConstants.ABOUT_US_URL)));
                break;
            case R.id.mButtonMyFavorite:
                AndyUtils.openActivity(getActivity(), MyFavoritesActivity.class);
                break;
            default:
                break;
        }
    }
}
