package com.agrability.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class RobotoRegularRadioButton extends android.support.v7.widget.AppCompatRadioButton {
    public RobotoRegularRadioButton(Context context) {
        super(context);
        init();
    }

    public RobotoRegularRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RobotoRegularRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "roboto/Roboto-Regular.ttf");
        setTypeface(tf);
    }
}
