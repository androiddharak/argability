package com.agrability.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.agrability.R;
import com.agrability.adapter.HomeFragmentListBaseAdapter;
import com.agrability.adapter.HomeSubCategoryAdapter;
import com.agrability.databinding.ActivityHomeSubBinding;
import com.agrability.model.GetCategories.CategoriesItem;
import com.agrability.model.GetCategories.GetCategoriesResponse;
import com.agrability.retrofit.APIClient;
import com.agrability.retrofit.APIInterface;
import com.agrability.utils.AndyConstants;
import com.agrability.utils.AndyUtils;
import com.agrability.utils.PreferenceHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeSubActivity extends AppCompatActivity {

    private static final String TAG = "HomeSubActivity";
    private ActivityHomeSubBinding activityHomeSubBinding;
    private PreferenceHelper preferenceHelper;
    private ArrayList<CategoriesItem> categoriesSubItemsList = new ArrayList<>();
    private int categoryId = AndyConstants.getIdOfMainCategory();
    private APIInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        preferenceHelper = new PreferenceHelper(this);
        activityHomeSubBinding = DataBindingUtil.setContentView(this, R.layout.activity_home_sub);
        setArrayList();

    }

    private void setArrayList() {
        for (CategoriesItem categoriesItem : preferenceHelper.getAllCategories().getCategories()) {
            if (categoryId == categoriesItem.getParent()) {
                categoriesSubItemsList.add(categoriesItem);
            }
        }
        activityHomeSubBinding.mTvTitle.setText(AndyConstants.getCategoryTitle());
        Log.d(TAG, "setArrayList:chintanyoooo ");
        activityHomeSubBinding.mGridViewSub.setAdapter(new HomeSubCategoryAdapter(this, categoriesSubItemsList));
        activityHomeSubBinding.mGridViewSub.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AndyConstants.setCategoryTitle(categoriesSubItemsList.get(position).getTitle());
                AndyConstants.setCategoryIDForSolution(categoriesSubItemsList.get(position).getId());
                AndyUtils.openActivity(HomeSubActivity.this, SubSubCategoriesActivity.class);
            }
        });
    }
}
