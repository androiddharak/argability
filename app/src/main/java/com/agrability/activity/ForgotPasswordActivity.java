package com.agrability.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.agrability.R;
import com.agrability.databinding.ActivityForgotPasswordBinding;
import com.agrability.model.forgotPassword.ForgotPasswordResponse;
import com.agrability.retrofit.APIClient;
import com.agrability.retrofit.APIInterface;
import com.agrability.utils.AndyUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {


    private ActivityForgotPasswordBinding binding;
    private APIInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        toolBar();
        initializeWidgets();
    }

    private void toolBar() {
        binding.toolBar.mTextViewToolbarTitle.setText(getString(R.string.forgot_password_title));
        binding.toolBar.mImageViewBack.setOnClickListener(this);
    }

    private void initializeWidgets() {
        binding.mButtonSubmit.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mImageViewBack:
                onBackPressed();
                break;
            case R.id.mButtonSubmit:
                if (binding.mEditTextEmail.getText().toString().trim().length() == 0) {
                    AndyUtils.showToast(getString(R.string.enter_email), ForgotPasswordActivity.this);
                } else if (!AndyUtils.eMailValidation(binding.mEditTextEmail.getText().toString().trim())) {
                    AndyUtils.showToast(getString(R.string.valid_email), ForgotPasswordActivity.this);
                } else {
                    callForgotPwd(binding.mEditTextEmail.getText().toString());
                }
                break;
            default:
                break;
        }
    }

    private void callForgotPwd(String email) {
        if (AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showSimpleProgressDialog(this);
            Call<ForgotPasswordResponse> loginCall = apiInterface.forgotPassword(email);
            loginCall.enqueue(new Callback<ForgotPasswordResponse>() {
                @Override
                public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {
                    AndyUtils.removeSimpleProgressDialog();
                    if (response.body() == null) {
                        AndyUtils.showToast("Empty Response", ForgotPasswordActivity.this);
                        return;
                    } else if (response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        onBackPressed();
                    } else if (!response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        AndyUtils.showToast("response not ok", ForgotPasswordActivity.this);
                    }
                }

                @Override
                public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {
                    AndyUtils.removeSimpleProgressDialog();
                }
            });
        } else {
            AndyUtils.DialogForNoInternetConnection(this);
        }
    }

}
