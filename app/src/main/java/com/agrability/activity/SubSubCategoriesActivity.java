package com.agrability.activity;

import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.agrability.R;
import com.agrability.adapter.ActivityAdapter;
import com.agrability.adapter.DisabilityAdapter;
import com.agrability.adapter.EnterpriseAdapter;
import com.agrability.adapter.FavoritesAdapter;
import com.agrability.adapter.SubSubCategoryAdapter;
import com.agrability.databinding.ActivitySubSubCategoriesBinding;
import com.agrability.model.FilterResponse.FilterResponse;
import com.agrability.model.GetCategories.CategoriesItem;
import com.agrability.model.GetFavoritesError.GetFavoritesErrorResponse;
import com.agrability.model.GetFavoritesResponse.GetFavoritesResponse;
import com.agrability.model.GetSolutionByCategory.GetSolutionByCategoryResponse;
import com.agrability.model.SolutionByCategoryResponse.MetaDataResponse;
import com.agrability.model.nonce.GenerateNonceFavorites;
import com.agrability.parse.AsyncTaskCompleteListener;
import com.agrability.parse.HttpRequester;
import com.agrability.retrofit.APIClient;
import com.agrability.retrofit.APIInterface;
import com.agrability.utils.AndyConstants;
import com.agrability.utils.AndyUtils;
import com.agrability.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubSubCategoriesActivity extends AppCompatActivity
        implements View.OnClickListener, AsyncTaskCompleteListener, AdapterView.OnItemSelectedListener {


    private static final String TAG = "SubSubCategoriesActivit";
    private ActivitySubSubCategoriesBinding subSubCategoriesBinding;
    private PreferenceHelper preferenceHelper;
    private ArrayList<CategoriesItem> activityList = new ArrayList<>();
    private ArrayList<CategoriesItem> enterpriseList = new ArrayList<>();
    private ArrayList<CategoriesItem> disabilityList = new ArrayList<>();
    private EnterpriseAdapter enterpriseAdapter;
    private ActivityAdapter activityAdapter;
    private DisabilityAdapter disabilityAdapter;
    private APIInterface apiInterface;
    private int categoryActivity = -2, categoryEnterprice = -2, categoryDisablity = -2;
//    private ArrayList<GetSolutionByCategoryResponse> getSolutionByCategoryResponseArrayList = new ArrayList<>();
//    private ArrayList<FilterResponse> filterResponseArrayList = new ArrayList<>();
    private ArrayList<MetaDataResponse> metaDataResponseArrayList = new ArrayList<>();
    public static ArrayList<GetFavoritesResponse> getFavoritesResponseArrayList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        preferenceHelper = new PreferenceHelper(this);
        subSubCategoriesBinding = DataBindingUtil.setContentView(this, R.layout.activity_sub_sub_categories);
        toolBar();
        setAllSpinnerList();

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GenerateNonceAsync().execute();
        getSolutionByCategory(String.valueOf(AndyConstants.getCategoryIDForSolution()), "title", "asc", "1", "50");
    }

    private void toolBar() {
        subSubCategoriesBinding.mTvTitle.setText(AndyConstants.getCategoryTitle());
        subSubCategoriesBinding.mIvBack.setOnClickListener(this);
    }

    private void setAllSpinnerList() {
        CategoriesItem categoriesItemNone = new CategoriesItem();
        categoriesItemNone.setTitle("none");
        categoriesItemNone.setId(-2);

        activityList.add(categoriesItemNone);
        enterpriseList.add(categoriesItemNone);
        disabilityList.add(categoriesItemNone);
        for (CategoriesItem categoriesItem : preferenceHelper.getAllCategories().getCategories()) {
            if (30 == categoriesItem.getParent()) {
                activityList.add(categoriesItem);
            } else if (categoriesItem.getParent() == 32) {
                enterpriseList.add(categoriesItem);
            } else if (categoriesItem.getParent() == 29) {
                disabilityList.add(categoriesItem);
            }
        }
        activityAdapter = new ActivityAdapter(SubSubCategoriesActivity.this, activityList);
        subSubCategoriesBinding.mSpinnerActivity.setAdapter(activityAdapter);

        enterpriseAdapter = new EnterpriseAdapter(SubSubCategoriesActivity.this, enterpriseList);
        subSubCategoriesBinding.mSpinnerEnterprise.setAdapter(enterpriseAdapter);

        disabilityAdapter = new DisabilityAdapter(SubSubCategoriesActivity.this, disabilityList);
        subSubCategoriesBinding.mSpinnerDisability.setAdapter(disabilityAdapter);
    }

    private void init() {
        subSubCategoriesBinding.mIvFilter.setOnClickListener(this);
        subSubCategoriesBinding.mIvDelete.setOnClickListener(this);
        subSubCategoriesBinding.mBtnApply.setOnClickListener(this);

        subSubCategoriesBinding.mSpinnerActivity.setOnItemSelectedListener(this);
        subSubCategoriesBinding.mSpinnerEnterprise.setOnItemSelectedListener(this);
        subSubCategoriesBinding.mSpinnerDisability.setOnItemSelectedListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mIvBack:
                onBackPressed();
                break;
            case R.id.mIvFilter:
                if (subSubCategoriesBinding.mLinearLayoutSpinner.getVisibility() == View.VISIBLE) {
                    subSubCategoriesBinding.mLinearLayoutSpinner.setVisibility(View.GONE);
                } else {
                    subSubCategoriesBinding.mLinearLayoutSpinner.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.mIvDelete:
                setAllSpinnerList();
                getSolutionByCategory(String.valueOf(AndyConstants.getCategoryIDForSolution()), "title", "asc", "1", "50");
                subSubCategoriesBinding.mLinearLayoutSpinner.setVisibility(View.GONE);
                break;
            case R.id.mBtnApply:
                if (categoryActivity != -2 || categoryEnterprice != -2 || categoryDisablity != -2) {
                    getFilter(categoryActivity != -2 ? String.valueOf(categoryActivity) : "",
                            categoryEnterprice != -2 ? String.valueOf(categoryEnterprice) : "",
                            categoryDisablity != -2 ? String.valueOf(categoryDisablity) : "");
                } else {
                    AndyUtils.showToast("Please select one of filter options", this);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        AndyUtils.removeSimpleProgressDialog();
        Log.d(TAG, "onTaskCompleted: " + response);
        switch (serviceCode) {
            case AndyConstants.ServiceCode.SOLUTION_BY_CATEGORY:
                if (response != null) {
                    metaDataResponseArrayList = getMetaDataInModel(response, metaDataResponseArrayList);
                    subSubCategoriesBinding.mGridViewSubSubCategory.setAdapter(new SubSubCategoryAdapter(this, metaDataResponseArrayList));
                    subSubCategoriesBinding.mLinearLayoutSpinner.setVisibility(View.GONE);
                } else {
                    AndyUtils.showToast(getResources().getString(R.string.strServerNot), this);
                }
                break;
            default:
                break;
        }
    }

    private ArrayList<MetaDataResponse> getMetaDataInModel(String response, ArrayList<MetaDataResponse> metaDataResponseArrayList) {
        metaDataResponseArrayList.clear();
        try {
            JSONArray jsonArrayMain = new JSONArray(response);
            for (int i = 0; i < jsonArrayMain.length(); i++) {
                MetaDataResponse metaDataResponse = new MetaDataResponse();
                JSONObject objectFirst = jsonArrayMain.optJSONObject(i);

                metaDataResponse.setId(objectFirst.optString("id"));
                JSONObject objectMeta = objectFirst.optJSONObject("meta");
                metaDataResponse.setSolution_name(objectMeta.getJSONArray("solution_name").getString(0));
                metaDataResponse.setSolution_nickname(objectMeta.getJSONArray("solution_nickname").getString(0));
                metaDataResponse.setSolution_description(objectMeta.getJSONArray("solution_description").getString(0));
                metaDataResponse.setSolution_website(objectMeta.getJSONArray("solution_website").getString(0));
                if(objectMeta.has("solution_retailer")) {
                    metaDataResponse.setSolution_retailer(objectMeta.getJSONArray("solution_retailer").getString(0));
                }
                metaDataResponse.setSolution_featured_image(objectFirst.optString("solution_featured_image"));
                metaDataResponseArrayList.add(metaDataResponse);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return metaDataResponseArrayList;
    }

    private void getSolutionByCategory(String categories, String orderby, String order, String page, String per_page) {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.removeSimpleProgressDialog();
            AndyUtils.DialogForNoInternetConnection(this);
            return;
        } else {
            AndyUtils.showSimpleProgressDialog(this);
            HashMap<String, String> map = new HashMap<>();
            if (AndyConstants.getIdOfMainCategory() == -1) {
                map.put(AndyConstants.URL, AndyConstants.Action.VIEW_ALL_CATEGORY);
            } else {
                map.put(AndyConstants.URL, AndyConstants.Action.SOLUTION_BY_CATEGORY + AndyConstants.Params.CATEGORIS + "=" + categories
                        + "&" + AndyConstants.Params.ORDER_BY + "=" + orderby + "&" + AndyConstants.Params.ORDER + "=" + order
                        + "&" + AndyConstants.Params.PAGE + "=" + page
                        + "&" + AndyConstants.Params.PER_PAGE + "=" + per_page);

            }
            new HttpRequester(this, map, AndyConstants.ServiceCode.SOLUTION_BY_CATEGORY, true, this);
        }
    }

    private void getFilter(String categoryActivity, String categoryEnterprice, String categoryDisablity) {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.removeSimpleProgressDialog();
            AndyUtils.DialogForNoInternetConnection(this);

            return;
        } else {
            AndyUtils.showSimpleProgressDialog(this);
            HashMap<String, String> map = new HashMap<>();
            if (AndyConstants.getIdOfMainCategory() == -1) {
                map.put(AndyConstants.URL, AndyConstants.Action.VIEW_ALL_CATEGORY);
            } else {
                map.put(AndyConstants.URL, AndyConstants.Action.FILTER_BY_CATEGORY
                        + AndyConstants.Params.CATEGORY_AND + "=" + categoryActivity
                        + "&" + AndyConstants.Params.CATEGORY_AND + "=" + categoryEnterprice
                        + "&" + AndyConstants.Params.CATEGORY_AND + "=" + categoryDisablity);
            }
            new HttpRequester(this, map, AndyConstants.ServiceCode.SOLUTION_BY_CATEGORY, true, this);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.mSpinnerActivity:
                categoryActivity = activityList.get(position).getId();
                break;
            case R.id.mSpinnerEnterprise:
                categoryEnterprice = enterpriseList.get(position).getId();
                break;
            case R.id.mSpinnerDisability:
                categoryDisablity = disabilityList.get(position).getId();
                break;
            default:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    //region API CALL For Favorites
    private void generateNonce() {
        if (AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showSimpleProgressDialog(this);
            Call<GenerateNonceFavorites> loginCall = apiInterface.generateNonceFavorite();
            loginCall.enqueue(new Callback<GenerateNonceFavorites>() {
                @Override
                public void onResponse(Call<GenerateNonceFavorites> call, Response<GenerateNonceFavorites> response) {
//                    AndyUtils.removeSimpleProgressDialog();
                    if (response.body() == null) {
                        AndyUtils.showToast("Empty Response", SubSubCategoriesActivity.this);
                        return;
                    } else if (response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        preferenceHelper.putFavNonce(response.body().getNonce());
                        getFavoriteList(response.body().getNonce(), preferenceHelper.getUserProfile().getCookie());
                    } else if (!response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        AndyUtils.showToast("response not ok", SubSubCategoriesActivity.this);
                    }
                }

                @Override
                public void onFailure(Call<GenerateNonceFavorites> call, Throwable t) {
                    AndyUtils.removeSimpleProgressDialog();
                }
            });
        } else {
            AndyUtils.DialogForNoInternetConnection(this);
        }
    }

    private void getFavoriteList(String nonce, String cookie) {
        if (AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showSimpleProgressDialog(this);
            Call<List<GetFavoritesResponse>> loginCall = apiInterface.getFavorites(nonce, cookie);
            loginCall.enqueue(new Callback<List<GetFavoritesResponse>>() {
                @Override
                public void onResponse(Call<List<GetFavoritesResponse>> call, Response<List<GetFavoritesResponse>> response) {
                    AndyUtils.removeSimpleProgressDialog();
                    if (response.body() == null) {
                        AndyUtils.showToast("Empty Response", SubSubCategoriesActivity.this);
                        return;
                    } else if (response.body().size() != 0 && response.body().get(0) != null) {
                        List<GetFavoritesResponse> getFavoritesResponse = (List<GetFavoritesResponse>) response.body();
                        Log.d(TAG, "onResponse:FAV1 "+getFavoritesResponse);
                        getFavoritesResponseArrayList.clear();
                        getFavoritesResponseArrayList.addAll(getFavoritesResponse);

                    } else if (response.body() instanceof GetFavoritesErrorResponse) {
                        GetFavoritesErrorResponse getFavoritesErrorResponse = (GetFavoritesErrorResponse) response.body();
                        AndyUtils.showToast(getFavoritesErrorResponse.getMessage(), SubSubCategoriesActivity.this);
                    } else {
                        Log.d(TAG, "onResponse:else ");
                    }
                }

                @Override
                public void onFailure(Call<List<GetFavoritesResponse>> call, Throwable t) {
                    AndyUtils.removeSimpleProgressDialog();
                    Log.d(TAG, "onFailure: "+t.getMessage());
                }
            });
        } else {
            AndyUtils.DialogForNoInternetConnection(this);
        }
    }

    private class GenerateNonceAsync extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... strings) {
            generateNonce();
            return null;
        }
    }
    //endregion

//    private void getSolutionByCategory(String categories, String orderby, String order, String page, String per_page) {
//        if (AndyUtils.isNetworkAvailable(this)) {
//            AndyUtils.showSimpleProgressDialog(this);
//            Call<List<FilterResponse>> loginCall = null;
//            if (AndyConstants.getIdOfMainCategory() == -1) {
//                loginCall = apiInterface.getViewAllCategories();
//            } else {
//                loginCall = apiInterface.getSolutionByCategory(categories,
//                        orderby, order, page, per_page);
//            }
//            loginCall.enqueue(new Callback<List<FilterResponse>>() {
//                @Override
//                public void onResponse(Call<List<FilterResponse>> call, Response<List<FilterResponse>> response) {
//                    AndyUtils.removeSimpleProgressDialog();
//                    if (response.body() == null) {
//                        AndyUtils.showToast("Empty Response", SubSubCategoriesActivity.this);
//                        return;
//                    } else if (response.body().size() != 0 && response.body().get(0) != null) {
//                        List<FilterResponse> getSolutionByCategoryResponses = response.body();
//                        filterResponseArrayList.addAll(getSolutionByCategoryResponses);
//                        subSubCategoriesBinding.mGridViewSubSubCategory.setAdapter(
//                                new SubSubCategoryAdapter(SubSubCategoriesActivity.this, filterResponseArrayList));
//                        subSubCategoriesBinding.mLinearLayoutSpinner.setVisibility(View.GONE);
//                    } else if (response.body() instanceof GetFavoritesErrorResponse) {
//                        GetFavoritesErrorResponse getFavoritesErrorResponse = (GetFavoritesErrorResponse) response.body();
//                        AndyUtils.showToast(getFavoritesErrorResponse.getMessage(), SubSubCategoriesActivity.this);
//                    } else {
//                        Log.d(TAG, "onResponse:else ");
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<List<FilterResponse>> call, Throwable t) {
//                    AndyUtils.removeSimpleProgressDialog();
//                    Log.d(TAG, "onFailure: " + t.getMessage());
//                }
//            });
//        } else {
//            AndyUtils.DialogForNoInternetConnection(this);
//        }
//    }
//
//    private void getFilter(String categoryActivity, String categoryEnterprice, String categoryDisablity) {
//        if (AndyUtils.isNetworkAvailable(this)) {
//            AndyUtils.showSimpleProgressDialog(this);
//            Call<List<FilterResponse>> loginCall = null;
//            if (AndyConstants.getIdOfMainCategory() == -1) {
//                loginCall = apiInterface.getViewAllCategories();
//            } else {
//                loginCall = apiInterface.getFilter(categoryActivity, categoryEnterprice, categoryDisablity);
//            }
//            try {
//                loginCall.enqueue(new Callback<List<FilterResponse>>() {
//                    @Override
//                    public void onResponse(Call<List<FilterResponse>> call, Response<List<FilterResponse>> response) {
//                        AndyUtils.removeSimpleProgressDialog();
//                        if (response.body() == null) {
//                            AndyUtils.showToast("Empty Response", SubSubCategoriesActivity.this);
//                            return;
//                        } else if (response.body().size() != 0 && response.body().get(0) != null) {
//                            List<FilterResponse> getSolutionByCategoryResponses = response.body();
//                            filterResponseArrayList.addAll(getSolutionByCategoryResponses);
//                            subSubCategoriesBinding.mGridViewSubSubCategory.setAdapter(
//                                    new SubSubCategoryAdapter(SubSubCategoriesActivity.this,
//                                            filterResponseArrayList));
//                            subSubCategoriesBinding.mLinearLayoutSpinner.setVisibility(View.GONE);
//                        } else if (response.body() instanceof GetFavoritesErrorResponse) {
//                            GetFavoritesErrorResponse getFavoritesErrorResponse = (GetFavoritesErrorResponse) response.body();
//                            AndyUtils.showToast(getFavoritesErrorResponse.getMessage(), SubSubCategoriesActivity.this);
//                        } else {
//                            Log.d(TAG, "onResponse:else ");
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<List<FilterResponse>> call, Throwable t) {
//                        AndyUtils.removeSimpleProgressDialog();
//                        Log.d(TAG, "onFailure: " + t.getMessage());
//                    }
//                });
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        } else {
//            AndyUtils.DialogForNoInternetConnection(this);
//        }
//    }
}
