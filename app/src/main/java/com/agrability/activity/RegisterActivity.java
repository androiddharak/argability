package com.agrability.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.agrability.R;
import com.agrability.databinding.ActivityRegisterBinding;
import com.agrability.model.register.RegisterStepOneResponse;
import com.agrability.model.register.RegisterStepTwoResponse;
import com.agrability.retrofit.APIClient;
import com.agrability.retrofit.APIInterface;
import com.agrability.utils.AndyUtils;
import com.agrability.utils.PreferenceHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "RegisterActivity";
    private ActivityRegisterBinding registerBinding;
    private APIInterface apiInterface;
    private PreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferenceHelper = new PreferenceHelper(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        registerBinding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        toolbar();
        init();
    }

    private void init() {
        registerBinding.mButtonSignUp.setOnClickListener(this);
    }

    private void toolbar() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mButtonSignUp:
                if (registerBinding.mEtEmail.getText().toString().trim().length() == 0) {
                    AndyUtils.showToast(getString(R.string.enter_email), RegisterActivity.this);
                } else if (!AndyUtils.eMailValidation(registerBinding.mEtEmail.getText().toString().trim())) {
                    AndyUtils.showToast(getString(R.string.valid_email), RegisterActivity.this);
                } else {
                    registerStepOne("user", "register", registerBinding.mEtEmail.getText().toString().trim());
                }
                break;
            default:
                break;
        }
    }

    private void registerStepOne(String controller, String method, final String username) {
        if (AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showSimpleProgressDialog(this);
            Call<RegisterStepOneResponse> loginCall = apiInterface.registerStepOne(controller, method);
            loginCall.enqueue(new Callback<RegisterStepOneResponse>() {
                @Override
                public void onResponse(Call<RegisterStepOneResponse> call, Response<RegisterStepOneResponse> response) {

                    if (response.body() == null) {
                        AndyUtils.showToast("Empty Response", RegisterActivity.this);
                    } else if (response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        registerStepTwo(response.body().getNonce(),username);
                    } else if (!response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        AndyUtils.showToast(response.body().getStatus(), RegisterActivity.this);
                    }
                }

                @Override
                public void onFailure(Call<RegisterStepOneResponse> call, Throwable t) {
                    AndyUtils.removeSimpleProgressDialog();
                }
            });
        } else {
            AndyUtils.DialogForNoInternetConnection(this);
        }
    }

    private void registerStepTwo(String nonce, String username) {
        if (AndyUtils.isNetworkAvailable(this)) {
            Call<RegisterStepTwoResponse> loginCall = apiInterface.registrationStepTwo(nonce, username, username, username);
            loginCall.enqueue(new Callback<RegisterStepTwoResponse>() {
                @Override
                public void onResponse(Call<RegisterStepTwoResponse> call, Response<RegisterStepTwoResponse> response) {
                    AndyUtils.removeSimpleProgressDialog();

                    Log.d(TAG, "onResponse:-->First "+response);
                    Log.d(TAG, "onResponse:-->Firstt "+response.errorBody());
                    Log.d(TAG, "onResponse:-->Firstt "+response.isSuccessful());
                    Log.d(TAG, "onResponse:-->Firstt "+response.raw());
                    Log.d(TAG, "onResponse:-->Firstt "+response.message());
                    if (response.body() == null) {
                        AndyUtils.showToast("Empty Response", RegisterActivity.this);
                        return;
                    } else if (response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        onBackPressed();
//                        preferenceHelper.putUserProfile(response.body());
//                        preferenceHelper.putIsLogin(true);
//                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(intent);
//                        finish();

                    } else if (!response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        AndyUtils.showToast(response.body().getStatus(), RegisterActivity.this);
                    }
                }

                @Override
                public void onFailure(Call<RegisterStepTwoResponse> call, Throwable t) {
                    AndyUtils.removeSimpleProgressDialog();
                }
            });
        } else {
            AndyUtils.DialogForNoInternetConnection(this);
        }
    }
}
