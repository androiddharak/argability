package com.agrability.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agrability.R;
import com.agrability.databinding.ActivityMainBinding;
import com.agrability.fragment.ContactUsFragment;
import com.agrability.fragment.HomeFragment;
import com.agrability.fragment.LinksFragment;
import com.agrability.fragment.SettingsFragment;
import com.agrability.fragment.VideoFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "MainActivity";
    private ActivityMainBinding activityMainBinding;
    private ArrayList<LinearLayout> arrayList = new ArrayList<>();
    private ArrayList<TextView> arrayListTextView = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        init();
        //loading the default fragment
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.mContainer, new HomeFragment());
//        transaction.commit();
        activityMainBinding.mLLHome.performClick();
    }

    private void init() {
        arrayList.add(activityMainBinding.mLLHome);
        arrayList.add(activityMainBinding.mLLLinks);
        arrayList.add(activityMainBinding.mLLContactUs);
        arrayList.add(activityMainBinding.mLLVideo);
        arrayList.add(activityMainBinding.mLLSettings);

        arrayListTextView.add(activityMainBinding.mTvHome);
        arrayListTextView.add(activityMainBinding.mTvLinks);
        arrayListTextView.add(activityMainBinding.mTvContactUs);
        arrayListTextView.add(activityMainBinding.mTvVideo);
        arrayListTextView.add(activityMainBinding.mTvSettings);

        activityMainBinding.mLLHome.setOnClickListener(this);
        activityMainBinding.mLLLinks.setOnClickListener(this);
        activityMainBinding.mLLContactUs.setOnClickListener(this);
        activityMainBinding.mLLVideo.setOnClickListener(this);
        activityMainBinding.mLLSettings.setOnClickListener(this);
    }

//    private void setBottomNavigation() {
//        activityMainBinding.mBottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
//        activityMainBinding.mBottomNavigation.enableAnimation(false);
//        activityMainBinding.mBottomNavigation.enableShiftingMode(false);
//        activityMainBinding.mBottomNavigation.enableItemShiftingMode(false);
//    }

    /*private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.bottom_navigation_one:
                    Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.mContainer);
                    if (!(currentFragment instanceof HomeFragment)) {
                        replaceFragment(new HomeFragment());
                    }
                    break;
                case R.id.bottom_navigation_two:
                    Fragment currentFragment1 = getSupportFragmentManager().findFragmentById(R.id.mContainer);
                    if (!(currentFragment1 instanceof LinksFragment)) {
                        replaceFragment(new LinksFragment());
                    }
                    break;
                case R.id.bottom_navigation_three:
                    Fragment currentFragment2 = getSupportFragmentManager().findFragmentById(R.id.mContainer);
                    if (!(currentFragment2 instanceof ContactUsFragment)) {
                        replaceFragment(new ContactUsFragment());
                    }
                    break;
                case R.id.bottom_navigation_four:
                    Fragment currentFragment3 = getSupportFragmentManager().findFragmentById(R.id.mContainer);
                    if (!(currentFragment3 instanceof VideoFragment)) {
                        replaceFragment(new VideoFragment());
                    }
                    break;
                case R.id.bottom_navigation_five:
                    Fragment currentFragment4 = getSupportFragmentManager().findFragmentById(R.id.mContainer);
                    if (!(currentFragment4 instanceof SettingsFragment)) {
                        replaceFragment(new SettingsFragment());
                    }
                    break;
            }
            return true;
        }
    };*/

    private void replaceFragment (Fragment fragment){
//        String backStateName =  fragment.getClass().getName();
//        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
//        boolean fragmentPopped = manager.popBackStackImmediate (backStateName, 0);

//        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null){ //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.mContainer, fragment, fragment.getClass().getName());
//            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

            ft.commit();
//            changeLayoutColor(activityMainBinding.mLLHome, activityMainBinding.mTvHome);
//        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mLLHome:
                changeLayoutColor(activityMainBinding.mLLHome, activityMainBinding.mTvHome);
                Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.mContainer);
                if (!(currentFragment instanceof HomeFragment)) {
                    replaceFragment(new HomeFragment());
                }
                break;
            case R.id.mLLLinks:
                changeLayoutColor(activityMainBinding.mLLLinks, activityMainBinding.mTvLinks);
                Fragment currentFragment1 = getSupportFragmentManager().findFragmentById(R.id.mContainer);
                if (!(currentFragment1 instanceof LinksFragment)) {
                    replaceFragment(new LinksFragment());
                }
                break;
            case R.id.mLLContactUs:
                changeLayoutColor(activityMainBinding.mLLContactUs, activityMainBinding.mTvContactUs);
                Fragment currentFragment2 = getSupportFragmentManager().findFragmentById(R.id.mContainer);
                if (!(currentFragment2 instanceof ContactUsFragment)) {
                    replaceFragment(new ContactUsFragment());
                }
                break;
            case R.id.mLLVideo:
                changeLayoutColor(activityMainBinding.mLLVideo, activityMainBinding.mTvVideo);
                Fragment currentFragment3 = getSupportFragmentManager().findFragmentById(R.id.mContainer);
                if (!(currentFragment3 instanceof VideoFragment)) {
                    replaceFragment(new VideoFragment());
                }
                break;
            case R.id.mLLSettings:
                changeLayoutColor(activityMainBinding.mLLSettings, activityMainBinding.mTvSettings);
                Fragment currentFragment4 = getSupportFragmentManager().findFragmentById(R.id.mContainer);
                if (!(currentFragment4 instanceof SettingsFragment)) {
                    replaceFragment(new SettingsFragment());
                }
                break;
        }
    }

    private void changeLayoutColor(LinearLayout mLLHome, TextView mTv) {
        for(LinearLayout layout : arrayList){
            if(layout.equals(mLLHome)){
                layout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            }else {
                layout.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            }
        }
        for(TextView mTextView : arrayListTextView){
            if(mTextView.equals(mTv)){
                mTextView.setTextColor(getResources().getColor(R.color.colorWhite));
            }else {
                mTextView.setTextColor(getResources().getColor(R.color.colorBlack));
            }
        }
    }
}
