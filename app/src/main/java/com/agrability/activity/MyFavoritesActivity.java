package com.agrability.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.agrability.R;
import com.agrability.adapter.FavoritesAdapter;
import com.agrability.adapter.HomeSubCategoryAdapter;
import com.agrability.databinding.ActivityMyFavoritesBinding;
import com.agrability.model.GetFavoritesError.GetFavoritesErrorResponse;
import com.agrability.model.GetFavoritesResponse.GetFavoritesResponse;
import com.agrability.model.nonce.GenerateNonceFavorites;
import com.agrability.retrofit.APIClient;
import com.agrability.retrofit.APIInterface;
import com.agrability.utils.AndyUtils;
import com.agrability.utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFavoritesActivity extends AppCompatActivity {

    private static final String TAG = "MyFavoritesActivity";
    private PreferenceHelper preferenceHelper;
    private APIInterface apiInterface;
    private ActivityMyFavoritesBinding myFavoritesBinding;
    private ArrayList<GetFavoritesResponse> getFavoritesResponseArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        preferenceHelper = new PreferenceHelper(this);
        myFavoritesBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_favorites);

        myFavoritesBinding.mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        generateNonce();
    }

    //region API CALL
    private void generateNonce() {
        if (AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showSimpleProgressDialog(this);
            Call<GenerateNonceFavorites> loginCall = apiInterface.generateNonceFavorite();
            loginCall.enqueue(new Callback<GenerateNonceFavorites>() {
                @Override
                public void onResponse(Call<GenerateNonceFavorites> call, Response<GenerateNonceFavorites> response) {
//                    AndyUtils.removeSimpleProgressDialog();
                    if (response.body() == null) {
                        AndyUtils.showToast("Empty Response", MyFavoritesActivity.this);
                        return;
                    } else if (response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        preferenceHelper.putFavNonce(response.body().getNonce());
                        getFavoriteList(response.body().getNonce(), preferenceHelper.getUserProfile().getCookie());
                    } else if (!response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        AndyUtils.showToast("response not ok", MyFavoritesActivity.this);
                    }
                }

                @Override
                public void onFailure(Call<GenerateNonceFavorites> call, Throwable t) {
                    AndyUtils.removeSimpleProgressDialog();
                }
            });
        } else {
            AndyUtils.DialogForNoInternetConnection(this);
        }
    }

    private void getFavoriteList(String nonce, String cookie) {
        if (AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showSimpleProgressDialog(this);
            Call<List<GetFavoritesResponse>> loginCall = apiInterface.getFavorites(nonce, cookie);
            loginCall.enqueue(new Callback<List<GetFavoritesResponse>>() {
                @Override
                public void onResponse(Call<List<GetFavoritesResponse>> call, Response<List<GetFavoritesResponse>> response) {
                    AndyUtils.removeSimpleProgressDialog();
                    if (response.body() == null) {
                        AndyUtils.showToast("Empty Response", MyFavoritesActivity.this);
                        return;
                    } else if (response.body().size() != 0 && response.body().get(0) != null) {
                        List<GetFavoritesResponse> getFavoritesResponse = (List<GetFavoritesResponse>) response.body();
                        Log.d(TAG, "onResponse:FAV1 "+getFavoritesResponse);
                        getFavoritesResponseArrayList.clear();
                        getFavoritesResponseArrayList.addAll(getFavoritesResponse);

                        Log.d(TAG, "onResponse:FAV2 "+getFavoritesResponseArrayList.size());
                        myFavoritesBinding.mGridViewFavorites.setAdapter(new FavoritesAdapter(MyFavoritesActivity.this,
                                getFavoritesResponseArrayList));
                    } else if (response.body() instanceof GetFavoritesErrorResponse) {
                        GetFavoritesErrorResponse getFavoritesErrorResponse = (GetFavoritesErrorResponse) response.body();
                        AndyUtils.showToast(getFavoritesErrorResponse.getMessage(), MyFavoritesActivity.this);
                    } else {
                        Log.d(TAG, "onResponse:else ");
                    }
                }

                @Override
                public void onFailure(Call<List<GetFavoritesResponse>> call, Throwable t) {

                    Log.d(TAG, "onFailure: "+t.getMessage());
                }
            });
        } else {
            AndyUtils.DialogForNoInternetConnection(this);
        }
    }
    //endregion
}
