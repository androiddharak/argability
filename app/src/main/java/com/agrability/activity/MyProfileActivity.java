package com.agrability.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.agrability.R;
import com.agrability.databinding.ActivityProfileBinding;
import com.agrability.model.GetUserInfo.GetUserInfoResponse;
import com.agrability.model.GetUserInfo.User;
import com.agrability.retrofit.APIClient;
import com.agrability.retrofit.APIInterface;
import com.agrability.utils.AndyUtils;
import com.agrability.utils.PreferenceHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MyProfileActivity";
    private ActivityProfileBinding activityProfileBinding;
    private PreferenceHelper preferenceHelper;
    private APIInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferenceHelper = new PreferenceHelper(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        activityProfileBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        init();
        getUserProfile();

    }

    private void getUserProfile() {
        if (AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showSimpleProgressDialog(this);
            Log.d(TAG, "getUserProfile: "+preferenceHelper.getUserProfile().getCookie());
            Call<GetUserInfoResponse> loginCall = apiInterface.getUserInfo(preferenceHelper.getUserProfile().getCookie());
            loginCall.enqueue(new Callback<GetUserInfoResponse>() {
                @Override
                public void onResponse(Call<GetUserInfoResponse> call, Response<GetUserInfoResponse> response) {
                    AndyUtils.removeSimpleProgressDialog();
                    if (response.body() == null) {
                        AndyUtils.showToast("Empty Response", MyProfileActivity.this);
                        return;
                    } else if (response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        setProfileData(response.body().getUser());
                    } else if (!response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        AndyUtils.showToast("response not ok", MyProfileActivity.this);
                    }
                }

                @Override
                public void onFailure(Call<GetUserInfoResponse> call, Throwable t) {
                    AndyUtils.removeSimpleProgressDialog();
                }
            });
        } else {
            AndyUtils.DialogForNoInternetConnection(this);
        }
    }

    private void setProfileData(User user) {
        activityProfileBinding.mEtFirstName.setText(user.getFirstname());
        activityProfileBinding.mEtLastName.setText(user.getLastname());
        activityProfileBinding.mEtEmail.setText(user.getEmail());
        activityProfileBinding.mEtWebSite.setText(user.getUrl());
//        activityProfileBinding.mEtCity.setText(user.get());
    }

    private void init() {
        activityProfileBinding.mEtBioGraphicalInformation.setImeOptions(EditorInfo.IME_ACTION_DONE);
        activityProfileBinding.mEtBioGraphicalInformation.setRawInputType(InputType.TYPE_CLASS_TEXT);

        activityProfileBinding.mIvBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mIvBack:
                onBackPressed();
                break;
            default:
                break;
        }
    }
}
