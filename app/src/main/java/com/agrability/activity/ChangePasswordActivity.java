package com.agrability.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.agrability.R;
import com.agrability.databinding.ActivityChangePasswordBinding;
import com.agrability.utils.AndyUtils;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityChangePasswordBinding activityChangePasswordBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityChangePasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        init();
    }

    private void init() {
        activityChangePasswordBinding.mButtonDone.setOnClickListener(this);
        activityChangePasswordBinding.mIvBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mButtonDone:
                if (activityChangePasswordBinding.mEtOldPassword.getText().toString().trim().length() == 0) {
                    AndyUtils.showToast(getString(R.string.strPlzOldPwd), this);
                } else if (activityChangePasswordBinding.mEtNewPassword.getText().toString().trim().length() == 0) {
                    AndyUtils.showToast(getString(R.string.strPlzpwd), this);
                } else if (activityChangePasswordBinding.mEtNewPassword.getText().toString().length() < 6) {
                    AndyUtils.showToast(getResources().getString(R.string.strPlzpwdValid), this);
                } else if (activityChangePasswordBinding.mEtConfirmPassword.getText().toString().length() == 0) {
                    AndyUtils.showToast(getResources().getString(R.string.strPlzconpwd), this);
                } else if (!activityChangePasswordBinding.mEtNewPassword.getText().toString().trim().
                        equalsIgnoreCase(activityChangePasswordBinding.mEtConfirmPassword.getText().toString().trim())) {
                    AndyUtils.showToast(getResources().getString(R.string.strBothPwdsame), this);
                } else {
                   /* changePassword(preferenceHelper.getUserId(),
                            activityChangePasswordBinding.mEtOldPassword.getText().toString().trim(),
                            activityChangePasswordBinding.mEtNewPassword.getText().toString().trim());*/
                }
                break;
            case R.id.mIvBack:
                onBackPressed();
                break;
            default:
                break;
        }
    }
}
