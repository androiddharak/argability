package com.agrability.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.agrability.R;
import com.agrability.databinding.ActivityLoginBinding;
import com.agrability.model.login.LoginResponse;
import com.agrability.retrofit.APIClient;
import com.agrability.retrofit.APIInterface;
import com.agrability.utils.AndyUtils;
import com.agrability.utils.PreferenceHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    private ActivityLoginBinding activityLoginBinding;
    private APIInterface apiInterface;
    private PreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferenceHelper = new PreferenceHelper(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if (preferenceHelper.getIsLogin()) {
            Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(mainIntent);
            finish();
        }
        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        toolBar();
        init();
    }

    private void init() {
        activityLoginBinding.mButtonSignIn.setOnClickListener(this);
        activityLoginBinding.mButtonSignUp.setOnClickListener(this);
        activityLoginBinding.mTvForgotPassword.setOnClickListener(this);
    }

    private void toolBar() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTvForgotPassword:
                AndyUtils.openActivity(this, ForgotPasswordActivity.class);
                break;
            case R.id.mButtonSignIn:
                if (activityLoginBinding.mEtEmail.getText().toString().trim().length() == 0) {
                    AndyUtils.showToast(getString(R.string.enter_email), LoginActivity.this);
                } else if (!AndyUtils.eMailValidation(activityLoginBinding.mEtEmail.getText().toString().trim())) {
                    AndyUtils.showToast(getString(R.string.valid_email), LoginActivity.this);
                } else if (activityLoginBinding.mEtPassword.getText().toString().trim().length() == 0) {
                    AndyUtils.showToast(getString(R.string.enter_pwd), LoginActivity.this);
                } else if (activityLoginBinding.mEtPassword.getText().toString().trim().length() < 6) {
                    AndyUtils.showToast(getString(R.string.valid_pwd), LoginActivity.this);
                } else {
                    makeLogin(activityLoginBinding.mEtEmail.getText().toString().trim(),
                            activityLoginBinding.mEtPassword.getText().toString().trim());
                }
                break;
            case R.id.mButtonSignUp:
                AndyUtils.openActivity(this,RegisterActivity.class);
                break;
            default:
                break;
        }
    }

    private void makeLogin(String username, String password) {
        if (AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showSimpleProgressDialog(this);
            Call<LoginResponse> loginCall = apiInterface.makeLogin(username, password);
            loginCall.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    AndyUtils.removeSimpleProgressDialog();
                    if (response.body() == null) {
                        AndyUtils.showToast("Empty Response", LoginActivity.this);
                    } else if (response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        preferenceHelper.putUserProfile(response.body());
                        preferenceHelper.putIsLogin(true);
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else if (!response.body().getStatus().equalsIgnoreCase(getString(R.string.ok))) {
                        AndyUtils.showToast("response not ok", LoginActivity.this);
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    AndyUtils.showToast("wrong credentials", LoginActivity.this);
                    AndyUtils.removeSimpleProgressDialog();
                }
            });
        } else {
            AndyUtils.DialogForNoInternetConnection(this);
        }
    }
}
