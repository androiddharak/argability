package com.agrability.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.agrability.R;
import com.agrability.databinding.ActivityCategoryDetailsBinding;
import com.agrability.model.SolutionByCategoryResponse.MetaDataResponse;
import com.agrability.parse.AsyncTaskCompleteListener;
import com.agrability.parse.HttpRequester;
import com.agrability.retrofit.APIClient;
import com.agrability.retrofit.APIInterface;
import com.agrability.utils.AndyConstants;
import com.agrability.utils.AndyUtils;
import com.agrability.utils.PreferenceHelper;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class CategoryDetailsActivity extends AppCompatActivity implements View.OnClickListener, AsyncTaskCompleteListener {

    private static final String TAG = "CategoryDetailsActivity";
    private ActivityCategoryDetailsBinding categoryDetailsBinding;
    private MetaDataResponse metaDataResponse = new MetaDataResponse();

    private PreferenceHelper preferenceHelper;
    private boolean callingAddtoFavorite = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferenceHelper = new PreferenceHelper(this);
        metaDataResponse = AndyConstants.getMetaDataObj();
        categoryDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_category_details);

        categoryDetailsBinding.mTvTitle.setText(!TextUtils.isEmpty(AndyConstants.getCategoryTitle()) ?
                AndyConstants.getCategoryTitle() : metaDataResponse.getSolution_name());
        Glide.with(this).load(metaDataResponse.getSolution_featured_image()).into(categoryDetailsBinding.mIvCategory);
        categoryDetailsBinding.mTvDesc.setText(Html.fromHtml(metaDataResponse.getSolution_description()));
        categoryDetailsBinding.mTvMoreInformation.setText(Html.fromHtml(metaDataResponse.getSolution_website()));
        categoryDetailsBinding.mTvStateWide.setText(Html.fromHtml(metaDataResponse.getSolution_retailer()));

        categoryDetailsBinding.mIvBack.setOnClickListener(this);
        categoryDetailsBinding.mIvFavorite.setOnClickListener(this);

        if (AndyConstants.isIsFavorite()) {
            categoryDetailsBinding.mIvFavorite.setImageResource(R.drawable.like);
        } else {
            categoryDetailsBinding.mIvFavorite.setImageResource(R.drawable.dislike);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mIvBack:
                onBackPressed();
                break;
            case R.id.mIvFavorite:
                if (AndyUtils.checkImageResource(this, categoryDetailsBinding.mIvFavorite, R.drawable.dislike)) {
                    callingAddtoFavorite = true;
                    addToFavorite(preferenceHelper.getUserProfile().getCookie(),
                            metaDataResponse.getId(), preferenceHelper.getFavNonce());
                } else {
                    callingAddtoFavorite = false;
                    addToFavorite(preferenceHelper.getUserProfile().getCookie(),
                            metaDataResponse.getId(), preferenceHelper.getFavNonce());
                }
                break;
        }
    }

    private void addToFavorite(String cookie, String post_id, String nonce) {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.removeSimpleProgressDialog();
            AndyUtils.DialogForNoInternetConnection(this);
            return;
        } else {
            AndyUtils.showSimpleProgressDialog(this);
            HashMap<String, String> map = new HashMap<>();
            if (callingAddtoFavorite) {
                map.put(AndyConstants.URL, AndyConstants.Action.ADD_TO_FAVORITE);
            } else {
                map.put(AndyConstants.URL, AndyConstants.Action.REMOVE_TO_FAVORITE);
            }
            map.put(AndyConstants.Params.COOKIE, cookie);
            map.put(AndyConstants.Params.POST_ID, post_id);
            map.put(AndyConstants.Params.NONCE, nonce);

            new HttpRequester(this, map, AndyConstants.ServiceCode.ADD_TO_FAVORITE, this);
        }

    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        Log.d(TAG, "ADD TO FAV: "+response);
        AndyUtils.removeSimpleProgressDialog();
        switch (serviceCode) {
            case AndyConstants.ServiceCode.ADD_TO_FAVORITE:
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has(AndyConstants.Params.STATUS) &&
                                jsonObject.optString(AndyConstants.Params.STATUS).equalsIgnoreCase("true")) {
                            AndyUtils.showToast(jsonObject.optString(AndyConstants.Params.MESSAGE), this);
                            if (callingAddtoFavorite) {
                                AndyConstants.setIsFavorite(true);
                            } else {
                                AndyConstants.setIsFavorite(false);
                            }
                            if (AndyConstants.isIsFavorite()) {
                                categoryDetailsBinding.mIvFavorite.setImageResource(R.drawable.like);
                            } else {
                                categoryDetailsBinding.mIvFavorite.setImageResource(R.drawable.dislike);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }
}
