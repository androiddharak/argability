package com.agrability.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.agrability.R;
import com.agrability.activity.LoginActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

@SuppressLint("NewApi")
public class AndyUtils {
    private static final String TAG = "AndyUtils";
    private static ProgressDialog mProgressDialog;
    private static Dialog mDialog;
    private static Bitmap myBitmap;
    private static PreferenceHelper preferenceHelper;

    public static void showSimpleProgressDialog(Context context, String title, String msg, boolean isCancelable) {
        try {
            if (mProgressDialog == null) {
                mProgressDialog = ProgressDialog.show(context, title, msg);
                mProgressDialog.setCancelable(isCancelable);
            }

            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showSimpleProgressDialog(Context context) {
        showSimpleProgressDialog(context, null, "Loading...", false);
    }

    public static void removeSimpleProgressDialog() {
        try {
            if (mProgressDialog != null) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void removeCustomProgressDialog() {
        try {
            if (mDialog != null) {
                mDialog.dismiss();
                mDialog = null;
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean eMailValidation(String emailstring) {
        if (null == emailstring || emailstring.length() == 0) {
            return false;
        }
        Pattern emailPattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher emailMatcher = emailPattern.matcher(emailstring);
        return emailMatcher.matches();
    }

    public static void showToast(String msg, Context ctx) {

        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }

    public static String convertToUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }

    public static void openAppInfoSettings(Context context) {
        Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + context.getPackageName()));
        context.startActivity(intent);
    }

    public static String takeScreenShot(final View view, final Context context) {
        String path = null;
        view.post(new Runnable() {
            public void run() {

                // take screenshot
                myBitmap = captureScreen(view);

                try {
                    if (myBitmap != null) {
                        // save image to SD card
                        saveImage(myBitmap, context);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        return path;
    }

    public static Bitmap captureScreen(View v) {

        Bitmap screenshot = null;
        try {

            if (v != null) {

                screenshot = Bitmap.createBitmap(v.getMeasuredWidth(),
                        v.getMeasuredHeight(), Config.ARGB_8888);
                Canvas canvas = new Canvas(screenshot);
                v.draw(canvas);
            }

        } catch (Exception e) {
            Log.d("ScreenShotActivity", "Failed to capture screenshot because:"
                    + e.getMessage());
        }

        return screenshot;
    }

    public static void saveImage(Bitmap bitmap, Context context)
            throws IOException {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + "/AdsGroup");
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        File f = new File(wallpaperDirectory, Calendar.getInstance()
                .getTimeInMillis() + ".jpg");
        f.createNewFile();
        FileOutputStream fo = new FileOutputStream(f);
        fo.write(bytes.toByteArray());
        MediaScannerConnection.scanFile(context, new String[]{f.getPath()},
                new String[]{"image/jpeg"}, null);
        fo.close();
        // new PreferenceHelper(context).putCardPath(f.getAbsolutePath());

    }

    public static final String getBase64String(Bitmap photoBitmap) {
        String photo;
        if (photoBitmap != null) {

            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            photoBitmap.compress(Bitmap.CompressFormat.PNG, 100, bao);
            // photoBitmap.recycle();
            photo = android.util.Base64.encodeToString(bao.toByteArray(),
                    android.util.Base64.DEFAULT);
            try {
                bao.close();
                bao = null;
                photoBitmap = null;

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            photo = "";
        }
        return photo;
    }

    public static void DialogForNoInternetConnection(final Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(
                "No Internet connection, Please Turn On Your Mobile Data or Wifi")
                .setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    final DialogInterface dialog,
                                    final int id) {
                                dialog.dismiss();
                                Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(dialogIntent);
                            }
                        });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    //  Method to open activity
    public static void openActivity(Context context, Class<?> cls, boolean shouldFinish) {
        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
        if (shouldFinish) {
            Activity activity = (Activity) context;
            activity.finish();
        }
    }

    public static MultipartBody.Part makeImageMultipartParam(String profile_picture, String parameter) {
        if (!TextUtils.isEmpty(profile_picture)) {
            File file = new File(profile_picture);
            // creates RequestBody instance from file
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            // MultipartBody.Part is used to send also the actual filename
            MultipartBody.Part profile_picture_body = MultipartBody.Part.createFormData(parameter, file.getName(), requestFile);
            return profile_picture_body;
        } else {
            return null;
        }
    }

    public static RequestBody makeTextRequestBody(String text) {
        if (!TextUtils.isEmpty(text)) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("text/plain"), text);
            return requestFile;
        } else {
            return null;
        }
    }

    public static void waitForXSecondsAndExecute(float numberOfSeconds, final Runnable runnable) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                runnable.run();
            }
        }, (long) (numberOfSeconds * 1000));
    }

    public static int[] getWidthHight(Activity mActivity) {
        int[] mWidthHight = new int[2];
        DisplayMetrics displayMetrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        mWidthHight[0] = width;
        mWidthHight[1] = height;

        return mWidthHight;
    }

    public static String makeFbProfileUrl(String fb_id) {
        String profile_picture = "https://graph.facebook.com/" + fb_id + "/picture?width=200&height=150";
        return profile_picture;
    }

    public static void LogoutDialog(final Activity activity, final PreferenceHelper preferenceHelper) {
        // Do footer action
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(
                "Are you sure you want to logout from this application?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    final DialogInterface dialog,
                                    final int id) {
                                preferenceHelper.onLogOut();
//                                if (LoginManager.getInstance() != null) {
//                                    LoginManager.getInstance().logOut();
//                                }
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                activity.finish();
                            }
                        }).
                setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    final DialogInterface dialog,
                                    final int id) {
                                dialog.cancel();
                            }
                        });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public static void composeEmail(String addresses, String subject, Activity activity) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{addresses});
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivity(intent);
        }
    }

    public static void shareAppLink(Activity activity) {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, activity.getResources().getString(R.string.app_name));
            i.putExtra(Intent.EXTRA_TEXT, "Please download best social media app on store : " + "https://play.google.com/store/apps/details?id=" + activity.getPackageName());
            activity.startActivity(Intent.createChooser(i, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    //    Method to get address from provided LatLong in parameter
    public static String getAddressFromLatLong(Context context, double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(", ");
                }
                strAdd = strReturnedAddress.toString();
                strAdd = strAdd.substring(0, strAdd.length() - 2);
            } else {
                Log.w("HarM", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("HarM", "Can not get Address!");
        }
        return strAdd;
    }

    public static void showErrorToastMessage(Context context, float time, String message) {
        AndyUtils.vibratePhone(context, time);
        AndyUtils.showToast(message, context);
    }

    public static void showErrorToastMessage(Context context, float time, String message, EditText editText) {
        AndyUtils.vibratePhone(context, time);
        AndyUtils.showToast(message, context);
        AndyUtils.setFocusOnEditText(context, editText);
    }


    public static void showConfirmationDialog(Context context, String message, String positiveButtonText, String negativeButtonText, final Runnable runnable) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(message);

        alertDialogBuilder.setPositiveButton(positiveButtonText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        runnable.run();
                    }
                });

        alertDialogBuilder.setNegativeButton(negativeButtonText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public static void showConfirmationDialog(Context context, String message, String positiveButtonText, final Runnable runnable) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(positiveButtonText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        runnable.run();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public static void reopenApp(Context context, Class<?> cls) {
        Intent mStartActivity = new Intent(context, cls);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }

    //  Method to set focus on EditText Widget and open Keyboard
    public static void setFocusOnEditText(Context context, EditText editText) {
        editText.requestFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }


    //  Method to vibrate phone for seconds set in "seconds" parameter
    public static void vibratePhone(Context context, float seconds) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate works on milliseconds therefore multiplied by 1000
        v.vibrate((long) (seconds * 1000));
    }


    //  Method to open activity
    public static void openActivity(Context context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
    }

    //  Method to open activity
    public static void openActivityAndClearPreviousStack(Context context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        clearActivityStack(intent);
        context.startActivity(intent);
    }

    /*
    must set below lines in style.xml between <style></style>
    * <item name="windowActionBar">false</item>
        <item name="windowNoTitle">true</item>
        <item name="android:windowLightStatusBar" tools:targetApi="m">true</item>
        <item name="android:windowDrawsSystemBarBackgrounds">true</item>
    * */

    //  To set colorbar of status, PARAMETER: activity, R.color.colorButtonPink
    public static void setStatusBarColor(Activity activity, int colorToSet) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(colorToSet));
        }
    }

    public static void hideKeyboardByDefault(Window window) {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private static void clearActivityStack(Intent intent) {
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }


    /*For set Drawable from string*/
    /*
      Context context = imageView.getContext();
      int id = context.getResources().getIdentifier("picture0001", "drawable", context.getPackageName());
      imageView.setImageResource(id);*/

    public static double round(double value, int places) {

        if (places < 0) throw new IllegalArgumentException();
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static int removeDecimal(double value) {
        int abcd = (int) value;
        Log.d(TAG, "round:--->" + abcd);
        return abcd;
    }

    public static String getlongtoago(String createdAt) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = null;
        try {
            date = formatter.parse(createdAt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        long currenttime = cal.getTimeInMillis();
        // Get msec from each, and subtract.
        long diff = currenttime - date.getTime();
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        String time = null;
        if (diffDays > 0) {
            if (diffDays == 1) {
                time = diffDays + " day ago ";
            } else {
                time = diffDays + " days ago ";
            }
        } else {
            if (diffHours > 0) {
                if (diffHours == 1) {
                    time = diffHours + " hr ago";
                } else {
                    time = diffHours + " hrs ago";
                }
            } else {
                if (diffMinutes > 0) {
                    if (diffMinutes == 1) {
                        time = diffMinutes + " min ago";
                    } else {
                        time = diffMinutes + " mins ago";
                    }
                } else {
                    if (diffSeconds > 0) {
                        time = diffSeconds + " secs ago";
                    }
                }

            }

        }
        return time;
    }


    public static String getDate(Date time) {
        SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy");
        String formattedDate = df.format(time);
        return formattedDate;
    }

    public static float roundFloat(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    public int compare(int x, int y) {
        if (x == y) {
            // Both numbers are odd or both numbers are even
            if (x == 0) {
                // Both numbers are even: compare as usual
                return Integer.compare(x, y);
            } else {
                // Both numbers are odd: compare in reverse
                return Integer.compare(y, x);
            }
        }
        // One is odd, the other one is even
        if (x == 0) {
            return -1;
        }
        return 1;
    }

   /* public static void showDatePickerDialog(final TextView mTextView, Activity activity, final Runnable runnable, boolean setMinimumDate) {
        final Calendar myCalendar = Calendar.getInstance();

        int daySelected, monthSelected, yearSelected;
        String[] split = mTextView.getText().toString().trim().split("-");
        *//*if (AndyConstants.isFromService) {
            daySelected = Integer.valueOf(split[2]);
            monthSelected = Integer.valueOf(split[1]);
            yearSelected = Integer.valueOf(split[0]);
        } else {*//*
        daySelected = Integer.valueOf(split[1]);
        monthSelected = Integer.valueOf(split[0]);
        yearSelected = Integer.valueOf(split[2]);
//        }
        final DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        //To do your task here
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateLabel(myCalendar, (TextView) mTextView, runnable);
                    }
                },
                yearSelected,
                monthSelected - 1,
                daySelected
        );

        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        if (setMinimumDate) {
            dpd.setMinDate(Calendar.getInstance());
        }
        dpd.show(activity.getFragmentManager(), "Datepickerdialog");
    }*/

    private static void updateLabel(Calendar myCalendar, TextView view, Runnable runnable) {
        String myFormat = "";
        /*if (AndyConstants.isFromService) {
            myFormat = "yyyy-MM-dd"; //In which you need put here
        } else {*/
        myFormat = "MM-dd-yyyy"; //In which you need put here
//        }
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        view.setText(sdf.format(myCalendar.getTime()));
        runnable.run();
    }

    public static String changeDateFormat(String date) {
        try {
            SimpleDateFormat spf = new SimpleDateFormat("MM-dd-yyyy");
            Date newDate = spf.parse(date);
            spf = new SimpleDateFormat("yyyy-MM-dd");
            date = spf.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
       /* DateFormat originalFormat = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = originalFormat.parse("August 21, 2012");
        String formattedDate = targetFormat.format(date);*/
        return date;
    }


    public static void showSoftKeyboard(final EditText editText, final Context context) {
        editText.requestFocus();

        editText.postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(editText, 0);
            }
        }, 200); //use 300 to make it run when coming back from lock screen
    }

    public static void hideKeyBoard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static boolean checkImageResource(Context ctx, ImageView imageView, int imageResource) {
        boolean result = false;

        if (ctx != null && imageView != null && imageView.getDrawable() != null) {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                constantState = ctx.getResources()
                        .getDrawable(imageResource, ctx.getTheme())
                        .getConstantState();
            } else {
                constantState = ctx.getResources().getDrawable(imageResource)
                        .getConstantState();
            }

            if (imageView.getDrawable().getConstantState() == constantState) {
                result = true;
            }
        }

        return result;
    }

    public static void shareWithEmail(Context context,String subject,String message) {
        Log.d(TAG, "shareWithEmail:1 ");
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
//        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
           context.startActivity(intent);
        }
        Log.d(TAG, "shareWithEmail:2 ");
    }
}
