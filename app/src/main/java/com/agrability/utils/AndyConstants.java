package com.agrability.utils;


import com.agrability.model.GetSolutionByCategory.GetSolutionByCategoryResponse;
import com.agrability.model.SolutionByCategoryResponse.MetaDataResponse;

public class AndyConstants {

//    public static final String BASE_URL = "http://zerones.biz/client/dr_eric_berg_keto/webapi/";
    public static final String BASE_URL = "https://030acda.netsolhost.com/6agrability/";
    public static final String BASE_URL_HTTP = "https://030acda.netsolhost.com/6agrability/";

    public static final String DEVICE_TYPE_ANDROID = "android";
    public static final String LOGIN_TYPE_CUSTOM = "Custom";
    public static final String LOGIN_TYPE_FB = "Facebook";
    public static final String PREF_NAME = "DrEricBerg";
    public static final String INITIAL_FOOD_QUANTITY = "0.0";
    public static final String LOGIN_TYPE_GOOGLE = "google";
    public static final String URL = "url";
    public static final String LINK_URL = "https://030acda.netsolhost.com/6agrability/";
    public static final String ABOUT_US_URL = "http://030acda.netsolhost.com/6agrability/about-kentucky-agrability/";
    public static final String API_YOUTUBE = "AIzaSyB66SowDypFb1Awyr93kDqhuDeAiB-w6yY";
    private static int idOfMainCategory;
    private static String categoryTitle;
    private static int categoryIDForSolution;
    private static boolean isFromFavoriteScreen = false;
    private static boolean isFavorite = false;
    private static MetaDataResponse metaDataObj;

    public static void setIdOfMainCategory(int idOfMainCategory) {
        AndyConstants.idOfMainCategory = idOfMainCategory;
    }

    public static int getIdOfMainCategory() {
        return idOfMainCategory;
    }

    public static void setCategoryTitle(String categoryTitle) {
        AndyConstants.categoryTitle = categoryTitle;
    }

    public static String getCategoryTitle() {
        return categoryTitle;
    }

    public static void setCategoryIDForSolution(int categoryIDForSolution) {
        AndyConstants.categoryIDForSolution = categoryIDForSolution;
    }

    public static int getCategoryIDForSolution() {
        return categoryIDForSolution;
    }

    public static void setMetaDataObj(MetaDataResponse metaDataObj) {
        AndyConstants.metaDataObj = metaDataObj;
    }

    public static MetaDataResponse getMetaDataObj() {
        return metaDataObj;
    }


    public static boolean isIsFromFavoriteScreen() {
        return isFromFavoriteScreen;
    }

    public static void setIsFromFavoriteScreen(boolean isFromFavoriteScreen) {
        AndyConstants.isFromFavoriteScreen = isFromFavoriteScreen;
    }


    public static boolean isIsFavorite() {
        return isFavorite;
    }

    public static void setIsFavorite(boolean isFavorite) {
        AndyConstants.isFavorite = isFavorite;
    }


    // web service url constants
    public class Action {
        public static final String REGISTRATION = "api/register.php";
        public static final String LOGIN = "api/auth/generate_auth_cookie/";
        public static final String FORGOT_PASSWORD = "api/user/retrieve_password/";
        public static final String REGISTER_STEP_ONE = "api/get_nonce/";
        public static final String REGISTER_STEP_TWO = "api/user/register/";
        public static final String GET_CATEGORIES = "api/get_category_index/";
        public static final String GET_USER_INFO = "api/user/get_currentuserinfo/";
        public static final String SOLUTION_BY_CATEGORY = BASE_URL_HTTP + "wp-json/wp/v2/solution?";
        public static final String VIEW_ALL_CATEGORY = BASE_URL_HTTP + "wp-json/wp/v2/allcategory";
        public static final String FILTER_BY_CATEGORY = BASE_URL_HTTP + "wp-json/wp/v2/soulutionbycat?";
        public static final String FILTER_VIDEO_BY_CATEGORY = BASE_URL_HTTP + "wp-json/wp/v2/videobycat?";
        public static final String GET_ALL_VIDEOS = BASE_URL_HTTP + "wp-json/wp/v2/allvideo";
        public static final String GENERATE_NONCE_FAVORITE = "api/get_nonce/?controller=auth&method=generate_auth_cookie";
        public static final String GET_FAVORITES = "wp-json/wp-favorite/v1/favorites?";
        public static final String ADD_TO_FAVORITE = BASE_URL_HTTP +  "wp-json/wp-favorite/v1/favorites/add";
        public static final String REMOVE_TO_FAVORITE = BASE_URL_HTTP +  "wp-json/wp-favorite/v1/favorites/remove";
    }
    public class ServiceCode {

        public static final int SOLUTION_BY_CATEGORY = 1;
        public static final int FILTER_BY_CATEGORY = 2;
        public static final int ADD_TO_FAVORITE = 3;
    }

    // webservice key constants
    public class Params {
        public static final String LOGIN_TYPE = "login_type";
        public static final String DEVICE_TYPE = "device_type";
        public static final String DEVICE_TOKEN = "device_token";
        public static final String EMAIL = "email";
        public static final String PASSWORD = "password";
        public static final String USERNAME = "username";
        public static final String FB = "FB";
        public static final String FB_ID = "fb_id";
        public static final String USERID = "user_id";
        public static final String CATEGORY_ID = "category_id";
        public static final String ITEM_ID = "item_id";
        public static final String DATE = "date";
        public static final String MEAL_PER_DAY = "meal_par_day";
        public static final String AMOUNT = "amount";
        public static final String FAT = "fat_total";
        public static final String FAT_GRAMS_TOTAL = "fat_grams_total";
        public static final String FAT_CALORIES_TOTAL = "fat_calories_total";
        public static final String PROTEIN_GRAMS_TOTAL = "protein_grams_total";
        public static final String PROTEIN_CALEROIES_TOTAL = "protein_calories_total";
        public static final String CARBS_GRAMS_TOTAL = "carbs_grams_total";
        public static final String CARBS_CALEROIES_TOTAL = "carbs_calories_total";
        public static final String SUGAR_GRAMS_TOTAL = "sugar_grams_total";
        public static final String FIBER_GRAMS_TOTAL = "fiber_grams_total";
        public static final String CALORIE_TOTAL = "calories_total";
        public static final String MEAL_ID = "meal_id";
        public static final String FAT_GRAM = "fat_grams";
        public static final String FAT_CALORIES = "fat_calories";
        public static final String PROTEIN_GRAM = "protein_grams";
        public static final String CARBS_GRAM = "carbs_grams";
        public static final String PROTEIN_CALORIES = "protein_calories";
        public static final String CARBS_CALORIES = "carbs_calories";
        public static final String FOOD_NAME = "item_name";
        public static final String TOTAL_CALORIES = "total_calories";
        public static final String FIBER_GRAM = "fiber_grams";
        public static final String SUGAR_GRAM = "sugar_grams";
        public static final String WEIGHT = "weight";
        public static final String WEIGHT_TYPE = "weight_unit";
        public static final String OLD_PASSWORD = "old_password";
        public static final String NEW_PASSWORD = "new_password";
        public static final String NEW_TO_KETO = "new_to_keto";
        public static final String FREQUENCY_OF_MEAL = "Frequency of Meals";
        public static final String ADD_YOUR_FOOD = "Add Your Food";
        public static final String USER_LOGIN = "user_login";
        public static final String METHOD = "method";
        public static final String CONTROLLER = "controller";
        public static final String NONCE = "nonce";
        public static final String DISPLAY_NAME = "display_name";
        public static final String COOKIE = "cookie";
        public static final String CATEGORIS = "categories";
        public static final String ORDER_BY = "orderby";
        public static final String ORDER = "order";
        public static final String PAGE = "page";
        public static final String PER_PAGE = "per_page";
        public static final String STATUS = "status";
        public static final String MESSAGE = "message";
        public static final String CATEGORY_AND = "category__and";
        public static final String POST_ID = "post_id";
    }
}

