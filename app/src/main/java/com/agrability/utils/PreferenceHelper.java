package com.agrability.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.agrability.model.GetCategories.GetCategoriesResponse;
import com.agrability.model.login.LoginResponse;
import com.google.gson.Gson;

public class PreferenceHelper {

    private static final String FAV_NONCE = "fav_nonce";
    private SharedPreferences app_prefs;
    private final String USER_ID = "user_id";
    private final String USER_TYPE = "user_type";
    private final String IS_LOGIN = "is_login";
    private final String FB_ID = "FB_ID";
    private final String IS_DISPLAY = "is_display";
    private final String EMAIL = "email";
    private final String USER_NAME = "userName";
    private final String DEVICE_TOKEN = "device_token";
    private final String IS_DISPLAY_HISTORY = "is_display_history";
    private final String IS_DISPLAY_CALORIE = "is_display_Calorie";
    private final String FIRST_NAME = "firstName";
    private final String LASTNAME = "lastName";
    private final String NEW_TO_KETO = "new_to_keto";
    private Context context;

    public PreferenceHelper(Context context) {
        app_prefs = context.getSharedPreferences(AndyConstants.PREF_NAME, Context.MODE_PRIVATE);
        this.context = context;
    }

    /*save model here*/

    public void putUserProfile(LoginResponse loginResponse) {
        Editor prefsEditor = app_prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(loginResponse); // myObject - instance of MyObject
        prefsEditor.putString("MyObject", json);
        prefsEditor.commit();
    }

    public LoginResponse getUserProfile() {
        Gson gson = new Gson();
        String json = app_prefs.getString("MyObject", "");
        LoginResponse obj = gson.fromJson(json, LoginResponse.class);
        return obj;
    }

    public void putAllCategories(GetCategoriesResponse getCategoriesResponse) {
        Editor prefsEditor = app_prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(getCategoriesResponse); // myObject - instance of MyObject
        prefsEditor.putString("AllCategories", json);
        prefsEditor.commit();
    }

    public GetCategoriesResponse getAllCategories() {
        Gson gson = new Gson();
        String json = app_prefs.getString("AllCategories", "");
        GetCategoriesResponse obj = gson.fromJson(json, GetCategoriesResponse.class);
        return obj;
    }

    public void putUserId(String userId) {
        Editor edit = app_prefs.edit();
        edit.putString(USER_ID, userId);
        edit.commit();
    }

    public String getUserId() {
        return app_prefs.getString(USER_ID, null);
    }

    public void putEmail(String email) {
        Editor edit = app_prefs.edit();
        edit.putString(EMAIL, email);
        edit.commit();
    }

    public String getEmail() {
        return app_prefs.getString(EMAIL, null);
    }

    public void putFirstName(String first) {
        Editor edit = app_prefs.edit();
        edit.putString(FIRST_NAME, first);
        edit.commit();
    }

    public String getFirstName() {
        return app_prefs.getString(FIRST_NAME, null);
    }

    public void putLastName(String first) {
        Editor edit = app_prefs.edit();
        edit.putString(LASTNAME, first);
        edit.commit();
    }

    public String getLastName() {
        return app_prefs.getString(LASTNAME, null);
    }

    public String getUsername() {
        return app_prefs.getString(USER_NAME, null);
    }

    public void putUsername(String uString) {
        Editor edit = app_prefs.edit();
        edit.putString(USER_NAME, uString);
        edit.commit();
    }

    public void putUserType(String user_type) {
        Editor edit = app_prefs.edit();
        edit.putString(USER_TYPE, user_type);
        edit.commit();
    }

    public String getUserType() {
        return app_prefs.getString(USER_TYPE, "");
    }

    public void putIsLogin(boolean isLogin) {
        Editor edit = app_prefs.edit();
        edit.putBoolean(IS_LOGIN, isLogin);
        edit.commit();
    }

    public Boolean getIsLogin() {
        return app_prefs.getBoolean(IS_LOGIN, false);
    }

    public void putDeviceToken(String refreshedToken) {
        Editor edit = app_prefs.edit();
        edit.putString(DEVICE_TOKEN, refreshedToken);
        edit.commit();
    }

    public String getDeviceToken() {
        return app_prefs.getString(DEVICE_TOKEN, "");
    }

    public void putFBID(String fb_id) {
        Editor edit = app_prefs.edit();
        edit.putString(FB_ID, fb_id);
        edit.commit();
    }

    public String getFBID() {
        return app_prefs.getString(FB_ID, "");
    }

    public void putIsDisplay(Boolean status) {
        Editor edit = app_prefs.edit();
        edit.putBoolean(IS_DISPLAY, status);
        edit.commit();
    }

    public boolean isDisplay() {
        return app_prefs.getBoolean(IS_DISPLAY, false);
    }

    public void putIsDisplayHistory(Boolean status) {
        Editor edit = app_prefs.edit();
        edit.putBoolean(IS_DISPLAY_HISTORY, status);
        edit.commit();
    }

    public boolean isDisplayHistory() {
        return app_prefs.getBoolean(IS_DISPLAY_HISTORY, false);
    }

    public void putIsDisplayCalorie(Boolean status) {
        Editor edit = app_prefs.edit();
        edit.putBoolean(IS_DISPLAY_CALORIE, status);
        edit.commit();
    }

    public boolean getIsDisplayCalorie() {
        return app_prefs.getBoolean(IS_DISPLAY_CALORIE, false);
    }

    public void putNewToKeto(String newToKeto) {
        Editor edit = app_prefs.edit();
        edit.putString(NEW_TO_KETO, newToKeto);
        edit.commit();
    }

    public String getNewToKeto() {
        return app_prefs.getString(NEW_TO_KETO, "");
    }

    public void putFavNonce(String nonce) {
        Editor edit = app_prefs.edit();
        edit.putString(FAV_NONCE, nonce);
        edit.commit();
    }

    public String getFavNonce() {
        return app_prefs.getString(FAV_NONCE, "");
    }

    public void onLogOut() {
        putUserId("");
        putFBID("");
        putUserType("");
        putEmail("");
        putUsername("");
        putFirstName("");
        putLastName("");
        putNewToKeto("");
        putFavNonce("");
        putIsLogin(false);
    }

}
