package com.agrability.model.register;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class RegisterStepTwoResponse{

	@SerializedName("cookie")
	private String cookie;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("status")
	private String status;

	public void setCookie(String cookie){
		this.cookie = cookie;
	}

	public String getCookie(){
		return cookie;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"RegisterStepTwoResponse{" + 
			"cookie = '" + cookie + '\'' + 
			",user_id = '" + userId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}