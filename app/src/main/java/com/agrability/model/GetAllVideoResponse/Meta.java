package com.agrability.model.GetAllVideoResponse;


public class Meta {

    private String videoUrl, videoTitle, youtubeVideoId, youtubeVideoEmbedCode;

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getYoutubeVideoId() {
        return youtubeVideoId;
    }

    public void setYoutubeVideoId(String youtubeVideoId) {
        this.youtubeVideoId = youtubeVideoId;
    }

    public String getYoutubeVideoEmbedCode() {
        return youtubeVideoEmbedCode;
    }

    public void setYoutubeVideoEmbedCode(String youtubeVideoEmbedCode) {
        this.youtubeVideoEmbedCode = youtubeVideoEmbedCode;
    }
}