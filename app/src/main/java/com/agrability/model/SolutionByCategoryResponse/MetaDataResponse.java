package com.agrability.model.SolutionByCategoryResponse;

public class MetaDataResponse {

    private String id;

//    public MetaDataResponse(String id, String solution_nickname, String solution_description,
//                            String solution_website, String solution_featured_image, String solution_name, String solution_retailer) {
//        this.id = id;
//        this.solution_nickname = solution_nickname;
//        this.solution_description = solution_description;
//        this.solution_website = solution_website;
//        this.solution_featured_image = solution_featured_image;
//        this.solution_name = solution_name;
//        this.solution_retailer = solution_retailer;
//    }

    private String solution_nickname;
    private String solution_description;
    private String solution_website;
    private String solution_featured_image;
    private String solution_name;
    private String solution_retailer;

    public String getSolution_name() {
        return solution_name;
    }

    public void setSolution_name(String solution_name) {
        this.solution_name = solution_name;
    }

    public String getSolution_featured_image() {
        return solution_featured_image;
    }

    public void setSolution_featured_image(String solution_featured_image) {
        this.solution_featured_image = solution_featured_image;
    }

    public String getSolution_website() {
        return solution_website;
    }

    public void setSolution_website(String solution_website) {
        this.solution_website = solution_website;
    }

    public String getSolution_description() {
        return solution_description;
    }

    public void setSolution_description(String solution_description) {
        this.solution_description = solution_description;
    }

    public String getSolution_nickname() {
        return solution_nickname;
    }

    public void setSolution_nickname(String solution_nickname) {
        this.solution_nickname = solution_nickname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSolution_retailer() {
        return solution_retailer;
    }

    public void setSolution_retailer(String solution_retailer) {
        this.solution_retailer = solution_retailer;
    }
}
