package com.agrability.model.GetUserInfo;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Capabilities{

	@SerializedName("subscriber")
	private boolean subscriber;

	public void setSubscriber(boolean subscriber){
		this.subscriber = subscriber;
	}

	public boolean isSubscriber(){
		return subscriber;
	}

	@Override
 	public String toString(){
		return 
			"Capabilities{" + 
			"subscriber = '" + subscriber + '\'' + 
			"}";
		}
}