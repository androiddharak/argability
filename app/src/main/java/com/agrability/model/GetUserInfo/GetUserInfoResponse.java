package com.agrability.model.GetUserInfo;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class GetUserInfoResponse{

	@SerializedName("user")
	private User user;

	@SerializedName("status")
	private String status;

	public void setUser(User user){
		this.user = user;
	}

	public User getUser(){
		return user;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"GetUserInfoResponse{" + 
			"user = '" + user + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}