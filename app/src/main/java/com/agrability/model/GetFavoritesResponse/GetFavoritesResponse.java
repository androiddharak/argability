package com.agrability.model.GetFavoritesResponse;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class GetFavoritesResponse{

	@SerializedName("comment_count")
	private String commentCount;

	@SerializedName("post_title")
	private String postTitle;

	@SerializedName("post_author")
	private String postAuthor;

	@SerializedName("menu_order")
	private int menuOrder;

	@SerializedName("pinged")
	private String pinged;

	@SerializedName("description")
	private String description;

	@SerializedName("post_excerpt")
	private String postExcerpt;

	@SerializedName("post_mime_type")
	private String postMimeType;

	@SerializedName("post_name")
	private String postName;

	@SerializedName("post_password")
	private String postPassword;

	@SerializedName("to_ping")
	private String toPing;

	@SerializedName("post_modified")
	private String postModified;

	@SerializedName("post_type")
	private String postType;

	@SerializedName("ID")
	private int iD;

	@SerializedName("post_status")
	private String postStatus;

	@SerializedName("post_content_filtered")
	private String postContentFiltered;

	@SerializedName("post_date_gmt")
	private String postDateGmt;

	@SerializedName("image")
	private List<String> image;

	@SerializedName("retailer")
	private String retailer;

	@SerializedName("solution_website")
	private String solutionWebsite;

	@SerializedName("post_modified_gmt")
	private String postModifiedGmt;

	@SerializedName("retailer_region1")
	private String retailerRegion1;

	@SerializedName("comment_status")
	private String commentStatus;

	@SerializedName("retailer_region2")
	private String retailerRegion2;

	@SerializedName("post_parent")
	private int postParent;

	@SerializedName("filter")
	private String filter;

	@SerializedName("post_content")
	private String postContent;

	@SerializedName("retailer_region3")
	private String retailerRegion3;

	@SerializedName("ping_status")
	private String pingStatus;

	@SerializedName("retailer_region4")
	private String retailerRegion4;

	@SerializedName("post_date")
	private String postDate;

	@SerializedName("guid")
	private String guid;

	public void setCommentCount(String commentCount){
		this.commentCount = commentCount;
	}

	public String getCommentCount(){
		return commentCount;
	}

	public void setPostTitle(String postTitle){
		this.postTitle = postTitle;
	}

	public String getPostTitle(){
		return postTitle;
	}

	public void setPostAuthor(String postAuthor){
		this.postAuthor = postAuthor;
	}

	public String getPostAuthor(){
		return postAuthor;
	}

	public void setMenuOrder(int menuOrder){
		this.menuOrder = menuOrder;
	}

	public int getMenuOrder(){
		return menuOrder;
	}

	public void setPinged(String pinged){
		this.pinged = pinged;
	}

	public String getPinged(){
		return pinged;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setPostExcerpt(String postExcerpt){
		this.postExcerpt = postExcerpt;
	}

	public String getPostExcerpt(){
		return postExcerpt;
	}

	public void setPostMimeType(String postMimeType){
		this.postMimeType = postMimeType;
	}

	public String getPostMimeType(){
		return postMimeType;
	}

	public void setPostName(String postName){
		this.postName = postName;
	}

	public String getPostName(){
		return postName;
	}

	public void setPostPassword(String postPassword){
		this.postPassword = postPassword;
	}

	public String getPostPassword(){
		return postPassword;
	}

	public void setToPing(String toPing){
		this.toPing = toPing;
	}

	public String getToPing(){
		return toPing;
	}

	public void setPostModified(String postModified){
		this.postModified = postModified;
	}

	public String getPostModified(){
		return postModified;
	}

	public void setPostType(String postType){
		this.postType = postType;
	}

	public String getPostType(){
		return postType;
	}

	public void setID(int iD){
		this.iD = iD;
	}

	public int getID(){
		return iD;
	}

	public void setPostStatus(String postStatus){
		this.postStatus = postStatus;
	}

	public String getPostStatus(){
		return postStatus;
	}

	public void setPostContentFiltered(String postContentFiltered){
		this.postContentFiltered = postContentFiltered;
	}

	public String getPostContentFiltered(){
		return postContentFiltered;
	}

	public void setPostDateGmt(String postDateGmt){
		this.postDateGmt = postDateGmt;
	}

	public String getPostDateGmt(){
		return postDateGmt;
	}

	public void setImage(List<String> image){
		this.image = image;
	}

	public List<String> getImage(){
		return image;
	}

	public void setRetailer(String retailer){
		this.retailer = retailer;
	}

	public String getRetailer(){
		return retailer;
	}

	public void setSolutionWebsite(String solutionWebsite){
		this.solutionWebsite = solutionWebsite;
	}

	public String getSolutionWebsite(){
		return solutionWebsite;
	}

	public void setPostModifiedGmt(String postModifiedGmt){
		this.postModifiedGmt = postModifiedGmt;
	}

	public String getPostModifiedGmt(){
		return postModifiedGmt;
	}

	public void setRetailerRegion1(String retailerRegion1){
		this.retailerRegion1 = retailerRegion1;
	}

	public String getRetailerRegion1(){
		return retailerRegion1;
	}

	public void setCommentStatus(String commentStatus){
		this.commentStatus = commentStatus;
	}

	public String getCommentStatus(){
		return commentStatus;
	}

	public void setRetailerRegion2(String retailerRegion2){
		this.retailerRegion2 = retailerRegion2;
	}

	public String getRetailerRegion2(){
		return retailerRegion2;
	}

	public void setPostParent(int postParent){
		this.postParent = postParent;
	}

	public int getPostParent(){
		return postParent;
	}

	public void setFilter(String filter){
		this.filter = filter;
	}

	public String getFilter(){
		return filter;
	}

	public void setPostContent(String postContent){
		this.postContent = postContent;
	}

	public String getPostContent(){
		return postContent;
	}

	public void setRetailerRegion3(String retailerRegion3){
		this.retailerRegion3 = retailerRegion3;
	}

	public String getRetailerRegion3(){
		return retailerRegion3;
	}

	public void setPingStatus(String pingStatus){
		this.pingStatus = pingStatus;
	}

	public String getPingStatus(){
		return pingStatus;
	}

	public void setRetailerRegion4(String retailerRegion4){
		this.retailerRegion4 = retailerRegion4;
	}

	public String getRetailerRegion4(){
		return retailerRegion4;
	}

	public void setPostDate(String postDate){
		this.postDate = postDate;
	}

	public String getPostDate(){
		return postDate;
	}

	public void setGuid(String guid){
		this.guid = guid;
	}

	public String getGuid(){
		return guid;
	}

	@Override
 	public String toString(){
		return 
			"GetFavoritesResponse{" + 
			"comment_count = '" + commentCount + '\'' + 
			",post_title = '" + postTitle + '\'' + 
			",post_author = '" + postAuthor + '\'' + 
			",menu_order = '" + menuOrder + '\'' + 
			",pinged = '" + pinged + '\'' + 
			",description = '" + description + '\'' + 
			",post_excerpt = '" + postExcerpt + '\'' + 
			",post_mime_type = '" + postMimeType + '\'' + 
			",post_name = '" + postName + '\'' + 
			",post_password = '" + postPassword + '\'' + 
			",to_ping = '" + toPing + '\'' + 
			",post_modified = '" + postModified + '\'' + 
			",post_type = '" + postType + '\'' + 
			",iD = '" + iD + '\'' + 
			",post_status = '" + postStatus + '\'' + 
			",post_content_filtered = '" + postContentFiltered + '\'' + 
			",post_date_gmt = '" + postDateGmt + '\'' + 
			",image = '" + image + '\'' + 
			",retailer = '" + retailer + '\'' + 
			",solution_website = '" + solutionWebsite + '\'' + 
			",post_modified_gmt = '" + postModifiedGmt + '\'' + 
			",retailer_region1 = '" + retailerRegion1 + '\'' + 
			",comment_status = '" + commentStatus + '\'' + 
			",retailer_region2 = '" + retailerRegion2 + '\'' + 
			",post_parent = '" + postParent + '\'' + 
			",filter = '" + filter + '\'' + 
			",post_content = '" + postContent + '\'' + 
			",retailer_region3 = '" + retailerRegion3 + '\'' + 
			",ping_status = '" + pingStatus + '\'' + 
			",retailer_region4 = '" + retailerRegion4 + '\'' + 
			",post_date = '" + postDate + '\'' + 
			",guid = '" + guid + '\'' + 
			"}";
		}
}