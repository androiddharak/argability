package com.agrability.model.FilterResponse;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Meta{

	@SerializedName("solution_retailer_region2")
	private List<String> solutionRetailerRegion2;

	@SerializedName("tax_input")
	private List<String> taxInput;

	@SerializedName("solution_retailer_region1")
	private List<String> solutionRetailerRegion1;

	@SerializedName("_pods_solution_additional_images")
	private List<String> podsSolutionAdditionalImages;

	@SerializedName("solution_retailer_region4")
	private List<String> solutionRetailerRegion4;

	@SerializedName("solution_retailer_region3")
	private List<String> solutionRetailerRegion3;

	@SerializedName("_edit_lock")
	private List<String> editLock;

	@SerializedName("solution_additional_images")
	private List<String> solutionAdditionalImages;

	@SerializedName("solution_description")
	private List<String> solutionDescription;

	@SerializedName("solution_website")
	private List<String> solutionWebsite;

	@SerializedName("_edit_last")
	private List<String> editLast;

	@SerializedName("_wpas_done_all")
	private List<String> wpasDoneAll;

	@SerializedName("solution_name")
	private List<String> solutionName;

	@SerializedName("solution_price_range_low")
	private List<String> solutionPriceRangeLow;

	@SerializedName("_pods_solution_image")
	private List<String> podsSolutionImage;

	@SerializedName("solution_instructions")
	private List<String> solutionInstructions;

	@SerializedName("solution_price_range_high")
	private List<String> solutionPriceRangeHigh;

	@SerializedName("solution_retailer")
	private List<String> solutionRetailer;

	@SerializedName("solution_image")
	private List<String> solutionImage;

	@SerializedName("edited_by")
	private List<String> editedBy;

	@SerializedName("solution_nickname")
	private List<String> solutionNickname;

	public void setSolutionRetailerRegion2(List<String> solutionRetailerRegion2){
		this.solutionRetailerRegion2 = solutionRetailerRegion2;
	}

	public List<String> getSolutionRetailerRegion2(){
		return solutionRetailerRegion2;
	}

	public void setTaxInput(List<String> taxInput){
		this.taxInput = taxInput;
	}

	public List<String> getTaxInput(){
		return taxInput;
	}

	public void setSolutionRetailerRegion1(List<String> solutionRetailerRegion1){
		this.solutionRetailerRegion1 = solutionRetailerRegion1;
	}

	public List<String> getSolutionRetailerRegion1(){
		return solutionRetailerRegion1;
	}

	public void setPodsSolutionAdditionalImages(List<String> podsSolutionAdditionalImages){
		this.podsSolutionAdditionalImages = podsSolutionAdditionalImages;
	}

	public List<String> getPodsSolutionAdditionalImages(){
		return podsSolutionAdditionalImages;
	}

	public void setSolutionRetailerRegion4(List<String> solutionRetailerRegion4){
		this.solutionRetailerRegion4 = solutionRetailerRegion4;
	}

	public List<String> getSolutionRetailerRegion4(){
		return solutionRetailerRegion4;
	}

	public void setSolutionRetailerRegion3(List<String> solutionRetailerRegion3){
		this.solutionRetailerRegion3 = solutionRetailerRegion3;
	}

	public List<String> getSolutionRetailerRegion3(){
		return solutionRetailerRegion3;
	}

	public void setEditLock(List<String> editLock){
		this.editLock = editLock;
	}

	public List<String> getEditLock(){
		return editLock;
	}

	public void setSolutionAdditionalImages(List<String> solutionAdditionalImages){
		this.solutionAdditionalImages = solutionAdditionalImages;
	}

	public List<String> getSolutionAdditionalImages(){
		return solutionAdditionalImages;
	}

	public void setSolutionDescription(List<String> solutionDescription){
		this.solutionDescription = solutionDescription;
	}

	public List<String> getSolutionDescription(){
		return solutionDescription;
	}

	public void setSolutionWebsite(List<String> solutionWebsite){
		this.solutionWebsite = solutionWebsite;
	}

	public List<String> getSolutionWebsite(){
		return solutionWebsite;
	}

	public void setEditLast(List<String> editLast){
		this.editLast = editLast;
	}

	public List<String> getEditLast(){
		return editLast;
	}

	public void setWpasDoneAll(List<String> wpasDoneAll){
		this.wpasDoneAll = wpasDoneAll;
	}

	public List<String> getWpasDoneAll(){
		return wpasDoneAll;
	}

	public void setSolutionName(List<String> solutionName){
		this.solutionName = solutionName;
	}

	public List<String> getSolutionName(){
		return solutionName;
	}

	public void setSolutionPriceRangeLow(List<String> solutionPriceRangeLow){
		this.solutionPriceRangeLow = solutionPriceRangeLow;
	}

	public List<String> getSolutionPriceRangeLow(){
		return solutionPriceRangeLow;
	}

	public void setPodsSolutionImage(List<String> podsSolutionImage){
		this.podsSolutionImage = podsSolutionImage;
	}

	public List<String> getPodsSolutionImage(){
		return podsSolutionImage;
	}

	public void setSolutionInstructions(List<String> solutionInstructions){
		this.solutionInstructions = solutionInstructions;
	}

	public List<String> getSolutionInstructions(){
		return solutionInstructions;
	}

	public void setSolutionPriceRangeHigh(List<String> solutionPriceRangeHigh){
		this.solutionPriceRangeHigh = solutionPriceRangeHigh;
	}

	public List<String> getSolutionPriceRangeHigh(){
		return solutionPriceRangeHigh;
	}

	public void setSolutionRetailer(List<String> solutionRetailer){
		this.solutionRetailer = solutionRetailer;
	}

	public List<String> getSolutionRetailer(){
		return solutionRetailer;
	}

	public void setSolutionImage(List<String> solutionImage){
		this.solutionImage = solutionImage;
	}

	public List<String> getSolutionImage(){
		return solutionImage;
	}

	public void setEditedBy(List<String> editedBy){
		this.editedBy = editedBy;
	}

	public List<String> getEditedBy(){
		return editedBy;
	}

	public void setSolutionNickname(List<String> solutionNickname){
		this.solutionNickname = solutionNickname;
	}

	public List<String> getSolutionNickname(){
		return solutionNickname;
	}

	@Override
 	public String toString(){
		return 
			"Meta{" + 
			"solution_retailer_region2 = '" + solutionRetailerRegion2 + '\'' + 
			",tax_input = '" + taxInput + '\'' + 
			",solution_retailer_region1 = '" + solutionRetailerRegion1 + '\'' + 
			",_pods_solution_additional_images = '" + podsSolutionAdditionalImages + '\'' + 
			",solution_retailer_region4 = '" + solutionRetailerRegion4 + '\'' + 
			",solution_retailer_region3 = '" + solutionRetailerRegion3 + '\'' + 
			",_edit_lock = '" + editLock + '\'' + 
			",solution_additional_images = '" + solutionAdditionalImages + '\'' + 
			",solution_description = '" + solutionDescription + '\'' + 
			",solution_website = '" + solutionWebsite + '\'' + 
			",_edit_last = '" + editLast + '\'' + 
			",_wpas_done_all = '" + wpasDoneAll + '\'' + 
			",solution_name = '" + solutionName + '\'' + 
			",solution_price_range_low = '" + solutionPriceRangeLow + '\'' + 
			",_pods_solution_image = '" + podsSolutionImage + '\'' + 
			",solution_instructions = '" + solutionInstructions + '\'' + 
			",solution_price_range_high = '" + solutionPriceRangeHigh + '\'' + 
			",solution_retailer = '" + solutionRetailer + '\'' + 
			",solution_image = '" + solutionImage + '\'' + 
			",edited_by = '" + editedBy + '\'' + 
			",solution_nickname = '" + solutionNickname + '\'' + 
			"}";
		}
}