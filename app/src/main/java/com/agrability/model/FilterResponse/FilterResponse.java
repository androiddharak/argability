package com.agrability.model.FilterResponse;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class FilterResponse{

	@SerializedName("comment_count")
	private String commentCount;

	@SerializedName("post_title")
	private String postTitle;

	@SerializedName("post_author")
	private String postAuthor;

	@SerializedName("menu_order")
	private int menuOrder;

	@SerializedName("pinged")
	private String pinged;

	@SerializedName("post_excerpt")
	private String postExcerpt;

	@SerializedName("post_mime_type")
	private String postMimeType;

	@SerializedName("post_name")
	private String postName;

	@SerializedName("post_password")
	private String postPassword;

	@SerializedName("to_ping")
	private String toPing;

	@SerializedName("post_modified")
	private String postModified;

	@SerializedName("post_type")
	private String postType;

	@SerializedName("ID")
	private int iD;

	@SerializedName("post_status")
	private String postStatus;

	@SerializedName("post_content_filtered")
	private String postContentFiltered;

	@SerializedName("post_date_gmt")
	private String postDateGmt;

	@SerializedName("solution_featured_image")
	private String solutionFeaturedImage;

	@SerializedName("post_modified_gmt")
	private String postModifiedGmt;

	@SerializedName("comment_status")
	private String commentStatus;

	@SerializedName("post_parent")
	private int postParent;

	@SerializedName("filter")
	private String filter;

	@SerializedName("post_content")
	private String postContent;

	@SerializedName("ping_status")
	private String pingStatus;

	@SerializedName("post_date")
	private String postDate;

	@SerializedName("meta")
	private Meta meta;

	@SerializedName("guid")
	private String guid;

	public void setCommentCount(String commentCount){
		this.commentCount = commentCount;
	}

	public String getCommentCount(){
		return commentCount;
	}

	public void setPostTitle(String postTitle){
		this.postTitle = postTitle;
	}

	public String getPostTitle(){
		return postTitle;
	}

	public void setPostAuthor(String postAuthor){
		this.postAuthor = postAuthor;
	}

	public String getPostAuthor(){
		return postAuthor;
	}

	public void setMenuOrder(int menuOrder){
		this.menuOrder = menuOrder;
	}

	public int getMenuOrder(){
		return menuOrder;
	}

	public void setPinged(String pinged){
		this.pinged = pinged;
	}

	public String getPinged(){
		return pinged;
	}

	public void setPostExcerpt(String postExcerpt){
		this.postExcerpt = postExcerpt;
	}

	public String getPostExcerpt(){
		return postExcerpt;
	}

	public void setPostMimeType(String postMimeType){
		this.postMimeType = postMimeType;
	}

	public String getPostMimeType(){
		return postMimeType;
	}

	public void setPostName(String postName){
		this.postName = postName;
	}

	public String getPostName(){
		return postName;
	}

	public void setPostPassword(String postPassword){
		this.postPassword = postPassword;
	}

	public String getPostPassword(){
		return postPassword;
	}

	public void setToPing(String toPing){
		this.toPing = toPing;
	}

	public String getToPing(){
		return toPing;
	}

	public void setPostModified(String postModified){
		this.postModified = postModified;
	}

	public String getPostModified(){
		return postModified;
	}

	public void setPostType(String postType){
		this.postType = postType;
	}

	public String getPostType(){
		return postType;
	}

	public void setID(int iD){
		this.iD = iD;
	}

	public int getID(){
		return iD;
	}

	public void setPostStatus(String postStatus){
		this.postStatus = postStatus;
	}

	public String getPostStatus(){
		return postStatus;
	}

	public void setPostContentFiltered(String postContentFiltered){
		this.postContentFiltered = postContentFiltered;
	}

	public String getPostContentFiltered(){
		return postContentFiltered;
	}

	public void setPostDateGmt(String postDateGmt){
		this.postDateGmt = postDateGmt;
	}

	public String getPostDateGmt(){
		return postDateGmt;
	}

	public void setSolutionFeaturedImage(String solutionFeaturedImage){
		this.solutionFeaturedImage = solutionFeaturedImage;
	}

	public String getSolutionFeaturedImage(){
		return solutionFeaturedImage;
	}

	public void setPostModifiedGmt(String postModifiedGmt){
		this.postModifiedGmt = postModifiedGmt;
	}

	public String getPostModifiedGmt(){
		return postModifiedGmt;
	}

	public void setCommentStatus(String commentStatus){
		this.commentStatus = commentStatus;
	}

	public String getCommentStatus(){
		return commentStatus;
	}

	public void setPostParent(int postParent){
		this.postParent = postParent;
	}

	public int getPostParent(){
		return postParent;
	}

	public void setFilter(String filter){
		this.filter = filter;
	}

	public String getFilter(){
		return filter;
	}

	public void setPostContent(String postContent){
		this.postContent = postContent;
	}

	public String getPostContent(){
		return postContent;
	}

	public void setPingStatus(String pingStatus){
		this.pingStatus = pingStatus;
	}

	public String getPingStatus(){
		return pingStatus;
	}

	public void setPostDate(String postDate){
		this.postDate = postDate;
	}

	public String getPostDate(){
		return postDate;
	}

	public void setMeta(Meta meta){
		this.meta = meta;
	}

	public Meta getMeta(){
		return meta;
	}

	public void setGuid(String guid){
		this.guid = guid;
	}

	public String getGuid(){
		return guid;
	}

	@Override
 	public String toString(){
		return 
			"FilterResponse{" + 
			"comment_count = '" + commentCount + '\'' + 
			",post_title = '" + postTitle + '\'' + 
			",post_author = '" + postAuthor + '\'' + 
			",menu_order = '" + menuOrder + '\'' + 
			",pinged = '" + pinged + '\'' + 
			",post_excerpt = '" + postExcerpt + '\'' + 
			",post_mime_type = '" + postMimeType + '\'' + 
			",post_name = '" + postName + '\'' + 
			",post_password = '" + postPassword + '\'' + 
			",to_ping = '" + toPing + '\'' + 
			",post_modified = '" + postModified + '\'' + 
			",post_type = '" + postType + '\'' + 
			",iD = '" + iD + '\'' + 
			",post_status = '" + postStatus + '\'' + 
			",post_content_filtered = '" + postContentFiltered + '\'' + 
			",post_date_gmt = '" + postDateGmt + '\'' + 
			",solution_featured_image = '" + solutionFeaturedImage + '\'' + 
			",post_modified_gmt = '" + postModifiedGmt + '\'' + 
			",comment_status = '" + commentStatus + '\'' + 
			",post_parent = '" + postParent + '\'' + 
			",filter = '" + filter + '\'' + 
			",post_content = '" + postContent + '\'' + 
			",ping_status = '" + pingStatus + '\'' + 
			",post_date = '" + postDate + '\'' + 
			",meta = '" + meta + '\'' + 
			",guid = '" + guid + '\'' + 
			"}";
		}
}