package com.agrability.model.GetCategories;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class GetCategoriesResponse{

	@SerializedName("count")
	private int count;

	@SerializedName("categories")
	private List<CategoriesItem> categories;

	@SerializedName("status")
	private String status;

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	public void setCategories(List<CategoriesItem> categories){
		this.categories = categories;
	}

	public List<CategoriesItem> getCategories(){
		return categories;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"GetCategoriesResponse{" + 
			"count = '" + count + '\'' + 
			",categories = '" + categories + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}