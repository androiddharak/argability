package com.agrability.model.login;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class LoginResponse{

	@SerializedName("cookie")
	private String cookie;

	@SerializedName("user")
	private User user;

	@SerializedName("status")
	private String status;

	@SerializedName("cookie_name")
	private String cookieName;

	public void setCookie(String cookie){
		this.cookie = cookie;
	}

	public String getCookie(){
		return cookie;
	}

	public void setUser(User user){
		this.user = user;
	}

	public User getUser(){
		return user;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setCookieName(String cookieName){
		this.cookieName = cookieName;
	}

	public String getCookieName(){
		return cookieName;
	}

	@Override
 	public String toString(){
		return 
			"LoginResponse{" + 
			"cookie = '" + cookie + '\'' + 
			",user = '" + user + '\'' + 
			",status = '" + status + '\'' + 
			",cookie_name = '" + cookieName + '\'' + 
			"}";
		}
}