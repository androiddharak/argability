package com.agrability.model.GetSolutionByCategory;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class GetSolutionByCategoryResponse{

	@SerializedName("date")
	private String date;

	@SerializedName("modified_gmt")
	private String modifiedGmt;

	@SerializedName("solution_featured_image")
	private String solutionFeaturedImage;

	@SerializedName("acf")
	private List<Object> acf;

	@SerializedName("_links")
	private Links links;

	@SerializedName("author")
	private int author;

	@SerializedName("link")
	private String link;

	@SerializedName("type")
	private String type;

	@SerializedName("title")
	private Title title;

	@SerializedName("content")
	private Content content;

	@SerializedName("featured_media")
	private int featuredMedia;

	@SerializedName("meta")
	private Meta meta;

	@SerializedName("guid")
	private Guid guid;

	@SerializedName("modified")
	private String modified;

	@SerializedName("id")
	private int id;

	@SerializedName("categories")
	private List<Integer> categories;

	@SerializedName("date_gmt")
	private String dateGmt;

	@SerializedName("slug")
	private String slug;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setModifiedGmt(String modifiedGmt){
		this.modifiedGmt = modifiedGmt;
	}

	public String getModifiedGmt(){
		return modifiedGmt;
	}

	public void setSolutionFeaturedImage(String solutionFeaturedImage){
		this.solutionFeaturedImage = solutionFeaturedImage;
	}

	public String getSolutionFeaturedImage(){
		return solutionFeaturedImage;
	}

	public void setAcf(List<Object> acf){
		this.acf = acf;
	}

	public List<Object> getAcf(){
		return acf;
	}

	public void setLinks(Links links){
		this.links = links;
	}

	public Links getLinks(){
		return links;
	}

	public void setAuthor(int author){
		this.author = author;
	}

	public int getAuthor(){
		return author;
	}

	public void setLink(String link){
		this.link = link;
	}

	public String getLink(){
		return link;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setTitle(Title title){
		this.title = title;
	}

	public Title getTitle(){
		return title;
	}

	public void setContent(Content content){
		this.content = content;
	}

	public Content getContent(){
		return content;
	}

	public void setFeaturedMedia(int featuredMedia){
		this.featuredMedia = featuredMedia;
	}

	public int getFeaturedMedia(){
		return featuredMedia;
	}

	public void setMeta(Meta meta){
		this.meta = meta;
	}

	public Meta getMeta(){
		return meta;
	}

	public void setGuid(Guid guid){
		this.guid = guid;
	}

	public Guid getGuid(){
		return guid;
	}

	public void setModified(String modified){
		this.modified = modified;
	}

	public String getModified(){
		return modified;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setCategories(List<Integer> categories){
		this.categories = categories;
	}

	public List<Integer> getCategories(){
		return categories;
	}

	public void setDateGmt(String dateGmt){
		this.dateGmt = dateGmt;
	}

	public String getDateGmt(){
		return dateGmt;
	}

	public void setSlug(String slug){
		this.slug = slug;
	}

	public String getSlug(){
		return slug;
	}

	@Override
 	public String toString(){
		return 
			"GetSolutionByCategoryResponse{" + 
			"date = '" + date + '\'' + 
			",modified_gmt = '" + modifiedGmt + '\'' + 
			",solution_featured_image = '" + solutionFeaturedImage + '\'' + 
			",acf = '" + acf + '\'' + 
			",_links = '" + links + '\'' + 
			",author = '" + author + '\'' + 
			",link = '" + link + '\'' + 
			",type = '" + type + '\'' + 
			",title = '" + title + '\'' + 
			",content = '" + content + '\'' + 
			",featured_media = '" + featuredMedia + '\'' + 
			",meta = '" + meta + '\'' + 
			",guid = '" + guid + '\'' + 
			",modified = '" + modified + '\'' + 
			",id = '" + id + '\'' + 
			",categories = '" + categories + '\'' + 
			",date_gmt = '" + dateGmt + '\'' + 
			",slug = '" + slug + '\'' + 
			"}";
		}
}