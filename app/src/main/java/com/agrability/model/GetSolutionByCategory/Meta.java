package com.agrability.model.GetSolutionByCategory;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Meta{

	@SerializedName("wpfp_favorites")
	private List<String> wpfpFavorites;

	@SerializedName("_edit_lock")
	private List<String> editLock;

	@SerializedName("solution_description")
	private List<String> solutionDescription;

	@SerializedName("solution_website")
	private List<String> solutionWebsite;

	@SerializedName("_edit_last")
	private List<String> editLast;

	@SerializedName("_cf_form_id")
	private List<String> cfFormId;

	@SerializedName("solution_name")
	private List<String> solutionName;

	@SerializedName("solution_price_range_low")
	private List<String> solutionPriceRangeLow;

	@SerializedName("_thumbnail_id")
	private List<String> thumbnailId;

	@SerializedName("_pods_solution_image")
	private List<String> podsSolutionImage;

	@SerializedName("solution_retailer")
	private List<String> solutionRetailer;

	@SerializedName("solution_price_range_high")
	private List<String> solutionPriceRangeHigh;

	@SerializedName("solution_image")
	private List<String> solutionImage;

	@SerializedName("solution_nickname")
	private List<String> solutionNickname;

	public void setWpfpFavorites(List<String> wpfpFavorites){
		this.wpfpFavorites = wpfpFavorites;
	}

	public List<String> getWpfpFavorites(){
		return wpfpFavorites;
	}

	public void setEditLock(List<String> editLock){
		this.editLock = editLock;
	}

	public List<String> getEditLock(){
		return editLock;
	}

	public void setSolutionDescription(List<String> solutionDescription){
		this.solutionDescription = solutionDescription;
	}

	public List<String> getSolutionDescription(){
		return solutionDescription;
	}

	public void setSolutionWebsite(List<String> solutionWebsite){
		this.solutionWebsite = solutionWebsite;
	}

	public List<String> getSolutionWebsite(){
		return solutionWebsite;
	}

	public void setEditLast(List<String> editLast){
		this.editLast = editLast;
	}

	public List<String> getEditLast(){
		return editLast;
	}

	public void setCfFormId(List<String> cfFormId){
		this.cfFormId = cfFormId;
	}

	public List<String> getCfFormId(){
		return cfFormId;
	}

	public void setSolutionName(List<String> solutionName){
		this.solutionName = solutionName;
	}

	public List<String> getSolutionName(){
		return solutionName;
	}

	public void setSolutionPriceRangeLow(List<String> solutionPriceRangeLow){
		this.solutionPriceRangeLow = solutionPriceRangeLow;
	}

	public List<String> getSolutionPriceRangeLow(){
		return solutionPriceRangeLow;
	}

	public void setThumbnailId(List<String> thumbnailId){
		this.thumbnailId = thumbnailId;
	}

	public List<String> getThumbnailId(){
		return thumbnailId;
	}

	public void setPodsSolutionImage(List<String> podsSolutionImage){
		this.podsSolutionImage = podsSolutionImage;
	}

	public List<String> getPodsSolutionImage(){
		return podsSolutionImage;
	}

	public void setSolutionRetailer(List<String> solutionRetailer){
		this.solutionRetailer = solutionRetailer;
	}

	public List<String> getSolutionRetailer(){
		return solutionRetailer;
	}

	public void setSolutionPriceRangeHigh(List<String> solutionPriceRangeHigh){
		this.solutionPriceRangeHigh = solutionPriceRangeHigh;
	}

	public List<String> getSolutionPriceRangeHigh(){
		return solutionPriceRangeHigh;
	}

	public void setSolutionImage(List<String> solutionImage){
		this.solutionImage = solutionImage;
	}

	public List<String> getSolutionImage(){
		return solutionImage;
	}

	public void setSolutionNickname(List<String> solutionNickname){
		this.solutionNickname = solutionNickname;
	}

	public List<String> getSolutionNickname(){
		return solutionNickname;
	}

	@Override
 	public String toString(){
		return 
			"Meta{" + 
			"wpfp_favorites = '" + wpfpFavorites + '\'' + 
			",_edit_lock = '" + editLock + '\'' + 
			",solution_description = '" + solutionDescription + '\'' + 
			",solution_website = '" + solutionWebsite + '\'' + 
			",_edit_last = '" + editLast + '\'' + 
			",_cf_form_id = '" + cfFormId + '\'' + 
			",solution_name = '" + solutionName + '\'' + 
			",solution_price_range_low = '" + solutionPriceRangeLow + '\'' + 
			",_thumbnail_id = '" + thumbnailId + '\'' + 
			",_pods_solution_image = '" + podsSolutionImage + '\'' + 
			",solution_retailer = '" + solutionRetailer + '\'' + 
			",solution_price_range_high = '" + solutionPriceRangeHigh + '\'' + 
			",solution_image = '" + solutionImage + '\'' + 
			",solution_nickname = '" + solutionNickname + '\'' + 
			"}";
		}
}