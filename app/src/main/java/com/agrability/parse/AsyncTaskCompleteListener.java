package com.agrability.parse;


public interface AsyncTaskCompleteListener {
	void onTaskCompleted(String response, int serviceCode);
}
