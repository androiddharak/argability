package com.agrability.parse;

import android.content.Context;
import android.util.Log;

import com.agrability.utils.AndyConstants;
import com.agrability.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class ParseContent {

    private static final String TAG = "ParseContent";
    private Context context;
    private PreferenceHelper preferenceHelper;
    public final String KEY_SUCCESS = "success";
    private String strmessage;


    public ParseContent(Context context) {
        this.context = context;
        preferenceHelper = new PreferenceHelper(context);
    }

    public boolean isSuccess(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            return jsonObject.getBoolean(AndyConstants.Params.STATUS);
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public String isnotSuccess(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (!jsonObject.getBoolean(AndyConstants.Params.STATUS)) {
                strmessage = jsonObject.getString(AndyConstants.Params.MESSAGE);
                return strmessage;
            } else {
                return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean parseLogin(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getBoolean(AndyConstants.Params.STATUS)) {


                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

}
