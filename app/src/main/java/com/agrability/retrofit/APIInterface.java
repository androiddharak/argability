package com.agrability.retrofit;


import com.agrability.model.FilterResponse.FilterResponse;
import com.agrability.model.GetCategories.GetCategoriesResponse;
import com.agrability.model.GetFavoritesResponse.GetFavoritesResponse;
import com.agrability.model.GetUserInfo.GetUserInfoResponse;
import com.agrability.model.forgotPassword.ForgotPasswordResponse;
import com.agrability.model.login.LoginResponse;
import com.agrability.model.nonce.GenerateNonceFavorites;
import com.agrability.model.register.RegisterStepOneResponse;
import com.agrability.model.register.RegisterStepTwoResponse;
import com.agrability.utils.AndyConstants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created  on 09/01/17.
 */

public interface APIInterface {


   /* @FormUrlEncoded
    @POST(AndyConstants.Action.LOGIN)
    Call<Login> getLogin(@Field(AndyConstants.Params.EMAIL) String email,
                         @Field(AndyConstants.Params.PASSWORD) String password,
                         @Field(AndyConstants.Params.LOGIN_TYPE) String login_type);*/

   /* @FormUrlEncoded
    @POST(AndyConstants.Action.GET_CATEGORY)
    Call<Category> getCategory(@Field(AndyConstants.Params.USERID) String userId);*/

    @FormUrlEncoded
    @POST(AndyConstants.Action.LOGIN)
    Call<LoginResponse> makeLogin(@Field(AndyConstants.Params.USERNAME) String username,
                                  @Field(AndyConstants.Params.PASSWORD) String password);


    @FormUrlEncoded
    @POST(AndyConstants.Action.FORGOT_PASSWORD)
    Call<ForgotPasswordResponse> forgotPassword(@Field(AndyConstants.Params.USER_LOGIN) String username);


    @FormUrlEncoded
    @POST(AndyConstants.Action.REGISTER_STEP_ONE)
    Call<RegisterStepOneResponse> registerStepOne(@Field(AndyConstants.Params.CONTROLLER) String controller,
                                                  @Field(AndyConstants.Params.METHOD) String method);


    @FormUrlEncoded
    @POST(AndyConstants.Action.REGISTER_STEP_TWO)
    Call<RegisterStepTwoResponse> registrationStepTwo(@Field(AndyConstants.Params.NONCE) String nonce,
                                                      @Field(AndyConstants.Params.USERNAME) String username,
                                                      @Field(AndyConstants.Params.EMAIL) String email,
                                                      @Field(AndyConstants.Params.DISPLAY_NAME) String display_name);

    @GET(AndyConstants.Action.GET_CATEGORIES)
    Call<GetCategoriesResponse> getAllCategories();


    @FormUrlEncoded
    @POST(AndyConstants.Action.GET_USER_INFO)
    Call<GetUserInfoResponse> getUserInfo(@Field(AndyConstants.Params.COOKIE) String cookie);


    @GET(AndyConstants.Action.GENERATE_NONCE_FAVORITE)
    Call<GenerateNonceFavorites> generateNonceFavorite();

    @GET(AndyConstants.Action.GET_FAVORITES)
    Call<List<GetFavoritesResponse>> getFavorites(@Query("nonce") String id,
                                                  @Query("cookie") String access_token);

    @GET(AndyConstants.Action.SOLUTION_BY_CATEGORY)
    Call<List<FilterResponse>> getSolutionByCategory(@Query(AndyConstants.Params.CATEGORIS) String categories,
                                                     @Query(AndyConstants.Params.ORDER_BY) String orderby,
                                                     @Query(AndyConstants.Params.ORDER) String order,
                                                     @Query(AndyConstants.Params.PAGE) String page,
                                                     @Query(AndyConstants.Params.PER_PAGE) String per_page);

    @GET(AndyConstants.Action.VIEW_ALL_CATEGORY)
    Call<List<FilterResponse>> getViewAllCategories();

    @GET(AndyConstants.Action.FILTER_BY_CATEGORY)
    Call<List<FilterResponse>> getFilter(@Query(AndyConstants.Params.CATEGORY_AND) String categoryActivity,
                                         @Query(AndyConstants.Params.CATEGORY_AND) String categoryEnterprice,
                                         @Query(AndyConstants.Params.CATEGORY_AND) String categoryDisablity);


   /* @FormUrlEncoded
    @POST(AndyConstants.Action.ADD_TO_FAVORITE)
    Call<List<AddRemoveFAvoriteResponse>> addToFavorite(@Field(AndyConstants.Params.COOKIE) String cookie,
                                                        @Field(AndyConstants.Params.POST_ID) String post_id,
                                                        @Field(AndyConstants.Params.NONCE) String nonce);

    @FormUrlEncoded
    @POST(AndyConstants.Action.REMOVE_TO_FAVORITE)
    Call<List<AddRemoveFAvoriteResponse>> removeToFavorite(@Field(AndyConstants.Params.COOKIE) String cookie,
                                                        @Field(AndyConstants.Params.POST_ID) String post_id,
                                                        @Field(AndyConstants.Params.NONCE) String nonce);*/


//    @GET(AndyConstants.Action.SOLUTION_BY_CATEGORY + AndyConstants.Params.CATEGORIS + "=" + "924"
//            + "&" + AndyConstants.Params.ORDER_BY + "=" + "title" + "&" + AndyConstants.Params.ORDER + "=" + "asc"
//            + "&" + AndyConstants.Params.PAGE + "=" + "1"
//            + "&" + AndyConstants.Params.PER_PAGE + "=" + "50")
//    Call<SolutionByCategoryResponse> getSolutionByCategory();
}
