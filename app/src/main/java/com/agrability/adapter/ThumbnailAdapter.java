/*
package com.agrability.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;

import com.agrability.databinding.VideoItemBinding;
import com.agrability.model.GetAllVideoResponse.Meta;

import java.util.ArrayList;

public class ThumbnailAdapter extends RecyclerView.Adapter<ThumbnailAdapter.ViewHolder> {

    private static final String TAG = "HistoryAdapter";
    private final Activity context;
    private ArrayList<Meta> metaArrayList;

    public ThumbnailAdapter(Activity context, ArrayList<Meta> metaArrayList) {
        this.context = context;
        this.metaArrayList = metaArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        VideoItemBinding binding = VideoItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ThumbnailAdapter.ViewHolder holder, final int position) {
        final ThumbnailAdapter.ViewHolder viewHolder = (ViewHolder) holder;
//        viewHolder.binding.mVideoPlayerView.initialize(AndyConstants.API_YOUTUBE, this);
//        viewHolder.binding.mVideoPlayerView.loadUrl(metaArrayList.get(position).getVideoUrl());

        viewHolder.binding.mVideoPlayerView.setInitialScale(1);
        viewHolder.binding.mVideoPlayerView.setWebChromeClient(new WebChromeClient());
        viewHolder.binding.mVideoPlayerView.getSettings().setAllowFileAccess(true);
//        viewHolder.binding.mVideoPlayerView.getSettings().setPluginState(WebSettings.PluginState.ON);
//        viewHolder.binding.mVideoPlayerView.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);
        viewHolder.binding.mVideoPlayerView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        viewHolder.binding.mVideoPlayerView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        viewHolder.binding.mVideoPlayerView.setWebViewClient(new WebViewClient());
        viewHolder.binding.mVideoPlayerView.getSettings().setJavaScriptEnabled(true);
        viewHolder.binding.mVideoPlayerView.getSettings().setLoadWithOverviewMode(true);
        viewHolder.binding.mVideoPlayerView.getSettings().setUseWideViewPort(true);
//        viewHolder.binding.mVideoPlayerView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//        viewHolder.binding.mVideoPlayerView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
//        viewHolder.binding.mVideoPlayerView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);

        viewHolder.binding.mVideoPlayerView.loadDataWithBaseURL("http://vimeo.com", metaArrayList.get(position).getYoutubeVideoEmbedCode()
                .replace("560", "1000").replace("315", "500"), "text/html", "UTF-8", null);

        viewHolder.binding.mVideoTitle.setText(metaArrayList.get(position).getVideoTitle());
        viewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return metaArrayList.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        VideoItemBinding binding;
        private ViewHolder(VideoItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

   */
/* @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult result) {

    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        *//*
*/
/** add listeners to YouTubePlayer instance **//*
*/
/*
        player.setPlayerStateChangeListener(playerStateChangeListener);
        player.setPlaybackEventListener(playbackEventListener);

        *//*
*/
/** Start buffering **//*
*/
/*
        if (!wasRestored) {
            player.cueVideo(metaArrayList.get(0).getYoutubeVideoId());
        }
    }

    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {

        @Override
        public void onBuffering(boolean arg0) {
        }

        @Override
        public void onPaused() {
        }

        @Override
        public void onPlaying() {
        }

        @Override
        public void onSeekTo(int arg0) {
        }

        @Override
        public void onStopped() {
        }

    };

    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {

        @Override
        public void onAdStarted() {
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
        }

        @Override
        public void onLoaded(String arg0) {
        }

        @Override
        public void onLoading() {
        }

        @Override
        public void onVideoEnded() {
        }

        @Override
        public void onVideoStarted() {
        }
    };*//*


}*/
