package com.agrability.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.agrability.R;
import com.agrability.databinding.HomeGridItemBinding;
import com.agrability.model.GetCategories.CategoriesItem;

import java.util.ArrayList;

public class HomeFragmentListBaseAdapter extends BaseAdapter {


    private final Activity mActivity;
    private final ArrayList<CategoriesItem> categoriesItems;

    public HomeFragmentListBaseAdapter(Activity activity, ArrayList<CategoriesItem> categoriesItems) {
        this.mActivity = activity;
        this.categoriesItems = categoriesItems;
    }

    @Override

    public int getCount() {
        return categoriesItems.size();
    }

    @Override
    public Object getItem(int position) {
        return categoriesItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HomeGridItemBinding homeGridItemBinding;

        if (convertView == null) {
            convertView = LayoutInflater.from(mActivity).inflate(R.layout.home_grid_item, null);
            homeGridItemBinding = DataBindingUtil.bind(convertView);
            convertView.setTag(homeGridItemBinding);
        } else {
            homeGridItemBinding = (HomeGridItemBinding) convertView.getTag();
        }

        homeGridItemBinding.mTvViewName.setText(categoriesItems.get(position).getTitle());

        homeGridItemBinding.executePendingBindings();
        return homeGridItemBinding.getRoot();
    }
}
