package com.agrability.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.agrability.R;
import com.agrability.activity.CategoryDetailsActivity;
import com.agrability.activity.SubSubCategoriesActivity;
import com.agrability.databinding.SubSubCategoryItemBinding;
import com.agrability.model.FilterResponse.FilterResponse;
import com.agrability.model.GetCategories.CategoriesItem;
import com.agrability.model.GetFavoritesResponse.GetFavoritesResponse;
import com.agrability.model.GetSolutionByCategory.GetSolutionByCategoryResponse;
import com.agrability.model.SolutionByCategoryResponse.MetaDataResponse;
import com.agrability.utils.AndyConstants;
import com.agrability.utils.AndyUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class SubSubCategoryAdapter extends BaseAdapter {


    private static final String TAG = "SubSubCategoryAdapter";
    private final Activity mActivity;
    //    private final ArrayList<FilterResponse> metaDataResponseArrayList;
    private final ArrayList<MetaDataResponse> metaDataResponseArrayList;

    //    public SubSubCategoryAdapter(Activity activity,
//                                 ArrayList<GetSolutionByCategoryResponse> metaDataResponseArrayList) {
//        this.mActivity = activity;
//        this.metaDataResponseArrayList = metaDataResponseArrayList;
//    }
//    public SubSubCategoryAdapter(Activity activity,
//                                 ArrayList<FilterResponse> metaDataResponseArrayList) {
//        this.mActivity = activity;
//        this.metaDataResponseArrayList = metaDataResponseArrayList;
//    }
    public SubSubCategoryAdapter(Activity activity, ArrayList<MetaDataResponse> metaDataResponseArrayList) {
        this.mActivity = activity;
        this.metaDataResponseArrayList = metaDataResponseArrayList;
    }

    @Override

    public int getCount() {
        return metaDataResponseArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return metaDataResponseArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        SubSubCategoryItemBinding subSubCategoryItemBinding;

        if (convertView == null) {
            convertView = LayoutInflater.from(mActivity).inflate(R.layout.sub_sub_category_item, null);
            subSubCategoryItemBinding = DataBindingUtil.bind(convertView);
            convertView.setTag(subSubCategoryItemBinding);
        } else {
            subSubCategoryItemBinding = (SubSubCategoryItemBinding) convertView.getTag();
        }

//        subSubCategoryItemBinding.mTvViewName.setText(metaDataResponseArrayList.get(position).getMeta().getSolutionNickname().get(0));
//        Glide.with(mActivity).load(metaDataResponseArrayList.get(position).getSolutionFeaturedImage()).into(subSubCategoryItemBinding.mIvImage);
        subSubCategoryItemBinding.mTvViewName.setText(metaDataResponseArrayList.get(position).getSolution_nickname());
        Glide.with(mActivity).load(metaDataResponseArrayList.get(position).getSolution_featured_image()).into(subSubCategoryItemBinding.mIvImage);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndyConstants.setIsFavorite(false);
                for (GetFavoritesResponse getFavoritesResponse :
                        SubSubCategoriesActivity.getFavoritesResponseArrayList) {
                    if (metaDataResponseArrayList.get(position).getId()
                            .equalsIgnoreCase(String.valueOf(getFavoritesResponse.getID()))) {
                        AndyConstants.setIsFavorite(true);
                    }
                }
                AndyConstants.setMetaDataObj(metaDataResponseArrayList.get(position));
                AndyConstants.setIsFromFavoriteScreen(false);
                AndyUtils.openActivity(mActivity, CategoryDetailsActivity.class);
            }
        });
        subSubCategoryItemBinding.executePendingBindings();
        return subSubCategoryItemBinding.getRoot();
    }
}


