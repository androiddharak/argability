package com.agrability.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.agrability.R;
import com.agrability.model.GetCategories.CategoriesItem;

import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class ActivityAdapter extends BaseAdapter {

    private static final String TAG = "DisabilityAdapter";
    private final Context context;
    private final ArrayList<CategoriesItem> arrayList;
    public TextView text;
    private LayoutInflater mInflator;

    public ActivityAdapter(Context context, ArrayList<CategoriesItem> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        mInflator = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflator.inflate(R.layout.custom_spinner_adapter, null);
        }
        text = convertView.findViewById(R.id.text);
        text.setText(arrayList.get(position).getTitle().replaceAll("[^\\p{ASCII}]", ""));
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }


    @Override
    public int getCount() {
        return arrayList.size();
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflator.inflate(R.layout.custom_spinner_adapter, null);
        }
        text = convertView.findViewById(R.id.text);
        text.setText(arrayList.get(position).getTitle().replaceAll("[^\\p{ASCII}]", ""));

        return convertView;
    }
}

