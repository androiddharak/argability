package com.agrability.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.agrability.R;
import com.agrability.activity.CategoryDetailsActivity;
import com.agrability.databinding.ItemFavoriteBinding;
import com.agrability.model.GetFavoritesResponse.GetFavoritesResponse;
import com.agrability.model.SolutionByCategoryResponse.MetaDataResponse;
import com.agrability.utils.AndyConstants;
import com.agrability.utils.AndyUtils;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class FavoritesAdapter extends BaseAdapter {

    private final Activity mActivity;
    private final ArrayList<GetFavoritesResponse> categoriesItems;

    public FavoritesAdapter(Activity activity, ArrayList<GetFavoritesResponse> categoriesItems) {
        this.mActivity = activity;
        this.categoriesItems = categoriesItems;
    }

    @Override

    public int getCount() {
        return categoriesItems.size();
    }

    @Override
    public Object getItem(int position) {
        return categoriesItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ItemFavoriteBinding itemFavoriteBinding;

        if (convertView == null) {
            convertView = LayoutInflater.from(mActivity).inflate(R.layout.item_favorite, null);
            itemFavoriteBinding = DataBindingUtil.bind(convertView);
            convertView.setTag(itemFavoriteBinding);
        } else {
            itemFavoriteBinding = (ItemFavoriteBinding) convertView.getTag();
        }

        itemFavoriteBinding.mTvViewName.setText(categoriesItems.get(position).getPostTitle());
        Glide.with(mActivity).load(categoriesItems.get(position).getImage().get(0))
                .into(itemFavoriteBinding.mIvImage);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MetaDataResponse metaDataResponse = new MetaDataResponse(
                       /* String.valueOf(categoriesItems.get(position).getID()),
                        categoriesItems.get(position).getPostTitle(),
                        categoriesItems.get(position).getDescription(),
                        categoriesItems.get(position).getSolutionWebsite(),
                        categoriesItems.get(position).getImage().get(0),
                        categoriesItems.get(position).getPostTitle(),
                        categoriesItems.get(position).getRetailer()*/
                );
                metaDataResponse.setId(String.valueOf(categoriesItems.get(position).getID()));
                metaDataResponse.setSolution_name(categoriesItems.get(position).getPostTitle());
                metaDataResponse.setSolution_nickname(categoriesItems.get(position).getPostTitle());
                metaDataResponse.setSolution_description(categoriesItems.get(position).getDescription());
                metaDataResponse.setSolution_website(categoriesItems.get(position).getSolutionWebsite());
                metaDataResponse.setSolution_retailer(categoriesItems.get(position).getRetailer());
                metaDataResponse.setSolution_featured_image( categoriesItems.get(position).getImage().get(0));

                AndyConstants.setMetaDataObj(metaDataResponse);
                AndyConstants.setIsFromFavoriteScreen(true);
                AndyConstants.setIsFavorite(true);
                AndyUtils.openActivity(mActivity, CategoryDetailsActivity.class);
            }
        });
        itemFavoriteBinding.executePendingBindings();
        return itemFavoriteBinding.getRoot();
    }
}


