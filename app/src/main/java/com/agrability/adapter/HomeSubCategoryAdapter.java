package com.agrability.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.agrability.R;
import com.agrability.databinding.HomeSubCategoryItemBinding;
import com.agrability.model.GetCategories.CategoriesItem;

import java.util.ArrayList;

public class HomeSubCategoryAdapter extends BaseAdapter {


    private final Activity mActivity;
    private final ArrayList<CategoriesItem> categoriesItems;

    public HomeSubCategoryAdapter(Activity activity, ArrayList<CategoriesItem> categoriesItems) {
        this.mActivity = activity;
        this.categoriesItems = categoriesItems;
    }

    @Override

    public int getCount() {
        return categoriesItems.size();
    }

    @Override
    public Object getItem(int position) {
        return categoriesItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HomeSubCategoryItemBinding homeSubCategoryItemBinding;

        if (convertView == null) {
            convertView = LayoutInflater.from(mActivity).inflate(R.layout.home_sub_category_item, null);
            homeSubCategoryItemBinding = DataBindingUtil.bind(convertView);
            convertView.setTag(homeSubCategoryItemBinding);
        } else {
            homeSubCategoryItemBinding = (HomeSubCategoryItemBinding) convertView.getTag();
        }

        homeSubCategoryItemBinding.mTvViewName.setText(categoriesItems.get(position).getTitle());

        homeSubCategoryItemBinding.executePendingBindings();
        return homeSubCategoryItemBinding.getRoot();
    }
}

